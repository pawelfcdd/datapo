# Requirements
To run this application on Your server, it must meet several requirements:
 - PHP >=7.2
 - MySQL >=5.6
 - NPM (latest version)

# How to install
  - Place an app code to the public folder of Your server (eg. `/var/www/html/`)
  - Create an application database
  - Go to `.env` file in the root dir of the application and set DB credentials in this line:
  `DATABASE_URL=mysql://DB_USER:mysql@DB_SERVER/DB_NAME`
  - Run installation file with the command `. install.sh` and follow the commands on the screen
   
