#!/usr/bin/env bash
echo "Install Symfony"
php bin/composer.phar self-update &&
php bin/composer.phar install &&
echo "Prepare database" &&
php bin/console doctrine:schema:create &&
echo "Create an admin user" &&
php bin/console fos:user:create --super-admin &&
php bin/console app:set-permissions &&
chmod -R 777 config/site_settings &&
echo "Install JS libs" &&
cd public/ && npm install &&
return 0;
