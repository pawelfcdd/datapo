<?php

namespace App\Command;

use App\Entity\KnowledgeSectionPermission;
use App\Entity\TaskSectionPermission;
use App\Entity\UploadsSectionPermission;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetPermissionCommand extends Command
{
    public static $defaultName = "app:set-permissions";
    public $em;
    private $taskSectionPermission;
    private $knowledgeSectionPermission;
    private $uploadSectionPermission;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->em = $entityManager;
        $this->taskSectionPermission = new TaskSectionPermission();
        $this->knowledgeSectionPermission = new KnowledgeSectionPermission();
        $this->uploadSectionPermission = new UploadsSectionPermission();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $dbUser = $this->em->getRepository(User::class)->findAll();
        /** @var User $dbUser */
        $dbUser = $dbUser[0];
        $this->taskSectionPermission->setUser($dbUser->getId())->setAccessLevel(2);
        $this->knowledgeSectionPermission->setUser($dbUser->getId())->setAccessLevel(2);
        $this->uploadSectionPermission->setUser($dbUser->getId())->setAccessLevel(2);
        $this->em->persist($this->taskSectionPermission);
        $this->em->persist($this->knowledgeSectionPermission);
        $this->em->persist($this->uploadSectionPermission);
        $this->em->flush();
    }
}