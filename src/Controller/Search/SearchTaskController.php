<?php

namespace App\Controller\Search;

use App\Controller\AppController;
use App\Entity\Task;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchTaskController extends AppController
{
    /**
     * @Route("/tasks/search", name="app.tasks_search")
     */
    public function findAction(Request $request)
    {
        $dateFromTime = strtotime($request->request->get('date-from'));
        $dateToTime = strtotime($request->request->get('date-to'));
        $dateFrom = $dateToTime ? date(self::TIME_FORMAT, $dateFromTime) : null;
        $dateTo = $dateToTime ? date(self::TIME_FORMAT, $dateToTime) : null;
        $statuses = $request->request->get('statuses');
        $user = $request->request->get('users');
        $keywords = $request->request->get('keywords');


        $tasks = $this->entityManager->getRepository(Task::class)
            ->taskSearchResults($this->getUser(), $dateFrom, $dateTo, $statuses, $user, $keywords);

        return $this->render('layouts/task/search_results.html.twig', [
           'tasks' => $tasks,
        ]);
    }
}