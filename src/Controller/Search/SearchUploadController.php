<?php

namespace App\Controller\Search;

use App\Controller\AppController;
use App\Entity\Upload;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchUploadController extends AppController
{
    /**
     * @Route("/uploads/search", name="app.upload_search")
     * @param Request $request
     */
    public function findAction(Request $request)
    {
        $dateFromTime = strtotime($request->request->get('date-from'));
        $dateToTime = strtotime($request->request->get('date-to'));

        $dateFrom = $dateToTime ? date(self::TIME_FORMAT, $dateFromTime) : null;
        $dateTo = $dateToTime ? date(self::TIME_FORMAT, $dateToTime) : null;
        $users = $request->request->get('users');
        $uploadId = $request->request->get('upload-id');
        $keywords = $request->request->get('keywords');

        $uploads = $this->entityManager->getRepository(Upload::class)
            ->searchTaskResults($this->getUser(), $dateFrom, $dateTo, $users, $keywords, $uploadId);

        return $this->render('layouts/uploads/search_results.html.twig', [
            'uploads' => $uploads
        ]);
    }
}