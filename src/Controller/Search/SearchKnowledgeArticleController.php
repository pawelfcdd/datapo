<?php

namespace App\Controller\Search;

use App\Controller\AppController;
use App\Entity\Knowledge;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchKnowledgeArticleController extends AppController
{
    /**
     * @Route("/knowledge/search", name="app.knowledge_search")
     * @param Request $request
     */
    public function findAction(Request $request)
    {
        $dateFromTime = strtotime($request->request->get('date-from'));
        $dateToTime = strtotime($request->request->get('date-to'));
        $dateFrom = $dateToTime ? date(self::TIME_FORMAT, $dateFromTime) : null;
        $dateTo = $dateToTime ? date(self::TIME_FORMAT, $dateToTime) : null;
        $user = $request->request->get('users');
        $keywords = $request->request->get('keywords');

       $articles = $this->entityManager->getRepository(Knowledge::class)
            ->searchTaskResults($this->getUser(), $dateFrom, $dateTo, $user, $keywords);

        return $this->render('layouts/knowledge/search_results.html.twig', [
            'articles' => $articles
        ]);
    }
}