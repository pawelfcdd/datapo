<?php

namespace App\Controller;

use App\Entity\Upload;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

class PublicFileController extends AbstractController
{
    /**
     * @Route("/file/{file}/authkey={accessToken}", name="app.file_public_link")
     * @param Upload $file
     * @param string $accessToken
     * @param Request $request
     *
     * @return static | Response
     */
    public function filePublicLink(Upload $file = null, string $accessToken, Request $request)
    {
        $filesAuthkey = Yaml::parseFile($this->getParameter('files_authkey'));

        if ($file && $filesAuthkey === $accessToken) {
            $filePath = $this->getParameter('kernel.project_dir') . '/public' .$file->getFile();
            if (file_exists($filePath)) {
                if (ob_get_level()) {
                    ob_end_clean();
                }
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($file->getId() . '.' . $file->getExtension()));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filePath));
                readfile($filePath);
                exit;
            }
            return Response::create('Файл не найден. Обратитесь к администратору');
        }
        return Response::create('Файл не найден. Обратитесь к администратору');
    }
}