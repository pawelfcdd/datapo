<?php

namespace App\Controller\Users;

use App\Controller\AppController;
use App\Entity\Interfaces\CategoryInterface;
use App\Entity\Interfaces\CategoryPermissionInterface;
use App\Entity\Interfaces\SectionPermissionInterface;
use App\Entity\KnowledgeCategory;
use App\Entity\KnowledgeCategoryPermission;
use App\Entity\KnowledgeSectionPermission;
use App\Entity\TaskCategory;
use App\Entity\TaskCategoryPermission;
use App\Entity\TaskSectionPermission;
use App\Entity\UploadCategory;
use App\Entity\UploadCategoryPermission;
use App\Entity\UploadsSectionPermission;
use App\Entity\User;
use App\Form\User\UserType;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UsersController
 * @package App\Controller\Users
 * @IsGranted("ROLE_SUPER_ADMIN")
 */
class UsersController extends AppController
{
    private $userManager;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, UserManagerInterface $userManager)
    {
        parent::__construct($entityManager, $session);
        $this->userManager = $userManager;
    }

    /**
     * @Route("/users", name="app.users_list")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $users = $this->entityManager->getRepository(User::class)->findBy(['deleted' => false]);

        return $this->render('layouts/users/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/users/add", name="app.users_add")
     *
     */
    public function addUserFormAction()
    {
        $form = $this->createForm(UserType::class, null, [
            'action' => $this->generateUrl('app.save_user')
        ]);

        $taskCategoriesList = $this->categoriesListAction(TaskCategory::class);
        $uploadCategoriesList = $this->categoriesListAction(UploadCategory::class);
        $knowledgeCategoriesList = $this->categoriesListAction(KnowledgeCategory::class);

        return $this->render('layouts/users/user_form.html.twig', [
            'form' => $form->createView(),
            'taskCategoriesList' => $taskCategoriesList,
            'uploadCategoriesList' => $uploadCategoriesList,
            'knowledgeCategoriesList' => $knowledgeCategoriesList
        ]);
    }

    /**
     * @Route("/users/save-user", name="app.save_user", methods={"POST"})
     */
    public function saveUserAction(Request $request)
    {
        $form = $this->createForm(UserType::class, null, [])->handleRequest($request);
        $user = $this->userManager->createUser();

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var User $formData */
            $formData = $form->getData();

            $user
                ->setEmail($formData->getEmail())
                ->addRole($request->request->get('role'))
                ->setUsername($formData->getEmail())
                ->setPlainPassword($request->request->get('user-password'))
                ->setEnabled(1)
                ->setName($formData->getName());

            $this->userManager->updateUser($user);

            $this->setSectionPermission(new TaskSectionPermission(), $request, 'tasks_section_', $user);
            $this->setSectionPermission(new UploadsSectionPermission(), $request, 'uploads_section_', $user);
            $this->setSectionPermission(new KnowledgeSectionPermission(), $request, 'knowledge_section_', $user);

            $this->setCategoryPermission(new TaskCategoryPermission(), new TaskCategory(), $request, 'task_category_', $user);
            $this->setCategoryPermission(new UploadCategoryPermission(), new UploadCategory(), $request, 'upload_category_', $user);
            $this->setCategoryPermission(new KnowledgeCategoryPermission(), new KnowledgeCategory(), $request, 'knowledge_category_', $user);


            return $this->redirectToRoute('app.users_list');
        }
    }

    /**
     * @Route("/users/{id}/edit", name="app.user_edit")
     *
     * @param User $user
     *
     * @return Response $response
     */
    public function editUserFormAction(User $user)
    {
        $disableRoleSelector = false;

        $form = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('app.user_update', [
                'id' => $user->getId()
            ]),
        ]);

        if ($user->getId() === $this->getUser()->getId()) {
            $disableRoleSelector = true;
        }

        $taskCategoriesList = $this->categoriesListAction(TaskCategory::class);
        $uploadCategoriesList = $this->categoriesListAction(UploadCategory::class);
        $knowledgeCategoriesList = $this->categoriesListAction(KnowledgeCategory::class);

        return $this->render('layouts/users/user_form.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'taskCategoriesList' => $taskCategoriesList,
            'uploadCategoriesList' => $uploadCategoriesList,
            'knowledgeCategoriesList' => $knowledgeCategoriesList,
            'disableRoleSelector' => $disableRoleSelector
        ]);
    }

    /**
     * @Route("/users/{id}/update-password", name="app.user_update_password", methods={"POST"})
     */
    public function updateUserPassword(User $user, Request $request)
    {
        $userPassword = $request->request->get('user')['password'];
        $user->setPlainPassword($userPassword);

        $this->userManager->updateUser($user);

        return $this->redirectToRoute('app.user_edit', ['id' => $user->getId()]);

    }

    /**
     * @Route("/users/{id}/update-user", name="app.user_update", methods={"POST"})
     * @param User $user
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateUserAction(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user, [])->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $formData */
            $formData = $form->getData();

            if (!empty($request->request->get('role'))) {
                if ($formData->getRoles()[0] !== $request->request->get('role')) {
                    $formData->setRoles([$request->request->get('role')]);
                }
            }

            $this->setSectionPermission(new TaskSectionPermission(), $request, 'tasks_section_', $user);
            $this->setSectionPermission(new UploadsSectionPermission(), $request, 'uploads_section_', $user);
            $this->setSectionPermission(new KnowledgeSectionPermission(), $request, 'knowledge_section_', $user);

            $this->setCategoryPermission(new TaskCategoryPermission(), new TaskCategory(), $request, 'task_category_', $user);
            $this->setCategoryPermission(new UploadCategoryPermission(), new UploadCategory(), $request, 'upload_category_', $user);
            $this->setCategoryPermission(new KnowledgeCategoryPermission(), new KnowledgeCategory(), $request, 'knowledge_category_', $user);

            $this->userManager->updateUser($formData);
            $this->entityManager->flush();
            return $this->redirectToRoute('app.user_edit', ['id' => $user->getId()]);
        }
    }

    /**
     * @Route("/users/user/{id}/delete", name="app.delete_user", methods={"POST"})
     *
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function deleteUserAction(User $user)
    {
        $deleteDate = date("Y_m_d_H_i_s");
        $user
            ->setUsername($user->getUsername() . '_' . $deleteDate)
            ->setUsernameCanonical($user->getUsername() . '_' . $deleteDate)
            ->setEmail($user->getEmail() . '_' . $deleteDate)
            ->setEmailCanonical($user->getEmailCanonical() . '_' . $deleteDate)
            ->setEnabled(false)
            ->setDeleted(true);

        $this->entityManager->flush();

        return $this->redirectToRoute('app.users_list');
    }

    /**
     * @param SectionPermissionInterface $sectionPermission
     * @param Request $request
     * @param string $sectionPrefix
     * @param User $user
     */
    public function setSectionPermission(
        SectionPermissionInterface $sectionPermission,
        Request $request,
        string $sectionPrefix,
        User $user
    )
    {

        foreach ($request->request->all() as $key => $val) {
            if (is_int(strpos($key, $sectionPrefix))) {
                /** @var SectionPermissionInterface $permission */
                $permission = $this->entityManager
                    ->getRepository(get_class($sectionPermission))
                    ->findOneBy(['user' => $user->getId()]);

                $val = $user->hasRole('ROLE_SUPER_ADMIN') ? 2 : $val;

                if (!$permission) {
                    $permission = $sectionPermission;
                }

                $permission
                    ->setUser($user->getId())
                    ->setAccessLevel($val);

                if (!$permission->getId()) {
                    $this->entityManager->persist($permission);
                }
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param CategoryPermissionInterface $categoryPermission
     * @param CategoryInterface $category
     * @param Request $request
     * @param string $categoryPrefix
     * @param User $user
     */
    public function setCategoryPermission(
        CategoryPermissionInterface $categoryPermission,
        CategoryInterface $category,
        Request $request,
        string $categoryPrefix,
        User $user
    )
    {

        $requestData = $request->request->all();

        foreach ($requestData as $key => $val) {
            if (is_int(strpos($key, $categoryPrefix))) {
                $taskCategoryId = str_replace($categoryPrefix, '', $key);
                $categoryPermissionFQN = get_class($categoryPermission);
                /** @var TaskCategoryPermission $permission */
                $permission = $this->entityManager
                    ->getRepository(get_class($categoryPermission))
                    ->findOneBy([
                        'category' => $taskCategoryId,
                        'user' => $user->getId(),
                    ]);


                if (!$permission) {
                    $permission = new $categoryPermissionFQN();
                }

                $val = $user->hasRole('ROLE_SUPER_ADMIN') ? 2 : $val;

                $permission
                    ->setCategory($taskCategoryId)
                    ->setUser($user->getId())
                    ->setAccessLevel($val);

                if (!$permission->getId()) {
                    $this->entityManager->persist($permission);
                }
            }
        }

        $this->entityManager->flush();
    }

    public function setPermissionsToChildCategory(TaskCategory $taskCategory, User $user, $accessLevel)
    {
        $taskCategoryPermission = new TaskCategoryPermission();

        $taskCategoryPermission
            ->setCategory($taskCategory->getId())
            ->setUser($user->getId())
            ->setAccessLevel($accessLevel);

        $this->entityManager->persist($taskCategoryPermission);

        if ($taskCategory->getChildren()) {
            $this->setPermissionsToChildCategory($taskCategory, $user, $accessLevel);
        }
    }


    public function categoriesListAction($categoryObjectFQN)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository($categoryObjectFQN)->findAll();
        $categoriesTree = [];
        foreach ($categories as $category) {
            if (!$category->getParent()) {
                $categoriesTree[$category->getId()]['parent'] = $category->getName();
                $categoriesTree[$category->getId()]['children'] = [];
            }
        }
        $tree = $this->renderNestedTree($categories, $categoriesTree);
        return $tree;
    }

    /**
     * @param array $categories
     * @param array $categoriesTree
     * @return array
     */
    private function renderNestedTree(array $categories, array $categoriesTree)
    {
        foreach ($categories as $category) {
            if ($category->getParent()) {
                if (array_key_exists($category->getParent()->getId(), $categoriesTree)) {
                    $parent = $category->getParent();
                    $categoriesTree[$parent->getId()]['children'][$category->getId()]['parent'] = $category->getName();
                    $categoriesTree[$parent->getId()]['children'][$category->getId()]['children'] = [];
                }
            }
        }
        foreach ($categoriesTree as $key => $treeItem) {
            if (!empty($treeItem['children'])) {
                $children = $this->renderNestedTree($categories, $treeItem['children']);
                $categoriesTree[$key]['children'] = $children;
            }
        }
        return $categoriesTree;
    }
}