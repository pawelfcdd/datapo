<?php

namespace App\Controller;

use App\Entity\Interfaces\CategoryInterface;
use App\Entity\Interfaces\CategoryPermissionInterface;
use App\Entity\Upload;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

class AppController extends AbstractController
{
    public const SELECTED_CATEGORY_ID = 'selected_category_id';

    public const SELECTED_CATEGORY_NAME = 'selected_category_name';

    public const TASK_STATUSES = [
        'new' => 'Новая',
        'in_progress' => 'В работе',
        'completed' => 'Завершенная',
        'declined' => 'Отмененная'
    ];

    public const USER_ROLES = [
        'ROLE_EDITOR' => 'Модератор',
        'ROLE_SUPER_ADMIN' => 'Администратор'
    ];

    public const USER_STATUS_SPAN_STYLES = [
        0 => 'border-grey text-grey',
        1 => 'border-success text-success'
    ];

    public const USER_STATUS_TEXTS = [
        0 => 'Заблокирован',
        1 => 'Активен'
    ];

    public const TIME_FORMAT = 'Y-m-d';

    public const ACCESS_LEVELS = [
        0 => '-',
        1 => 'Просматривать',
        2 => 'Редактировать'
    ];
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    public $entityManager;

    /**
     * @var SessionInterface
     */
    public $session;
    public $breadcrumbsArray;
    public $availableIds = [];

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public function checkSectionAccess(User $user, $sectionPermissionEntityFQN)
    {
        $sectionPermission = $this->entityManager->getRepository($sectionPermissionEntityFQN)
            ->findOneBy(['user' => $user->getId()]);

        if (!$sectionPermission) {
            return false;
        }

        return $sectionPermission->getAccessLevel();
    }

    /**
     * @param CategoryInterface $category
     * @param array $tree
     *
     * @return array
     */
    public function generateBreadcrumbs(CategoryInterface $category, array $tree = [])
    {

        if ($category->getParent()) {
            $tree[$category->getParent()->getId()] = $category->getParent()->getName();
            $this->generateBreadcrumbs($category->getParent(), $tree);
        }

        if (!$category->getParent()) {
            $this->breadcrumbsArray = array_reverse(array_flip($tree));
        }

        return $this->breadcrumbsArray;
    }

    /**
     * @param CategoryPermissionInterface $categoryPermission
     * @param CategoryInterface $category
     */
    public function setNewCategoryPermission(CategoryPermissionInterface $categoryPermission, CategoryInterface $category)
    {
        $users = $this->entityManager->getRepository(User::class)->findBy(['deleted' => false]);

        /** @var User $user */
        foreach ($users as $user) {
            $permissionFQN = get_class($categoryPermission);
            $permission = new $permissionFQN();

            $permission
                ->setUser($user->getId())
                ->setCategory($category->getId())
                ->setAccessLevel($user->hasRole('ROLE_SUPER_ADMIN') ? 2 : 1);

            $this->entityManager->persist($permission);
        }

        $this->entityManager->flush();
    }

    public function checkCategoryAccess(User $user, $categoryPermissionEntityFQN, CategoryInterface $category)
    {

        if ($category->isHidden()) {
            if (!$user->hasRole('ROLE_SUPER_ADMIN'))
                return false;
        }

        $categoryPermission = $this->entityManager->getRepository($categoryPermissionEntityFQN)
            ->findOneBy([
                'category' => $category->getId(),
                'user' => $user->getId()
            ]);

        if (!$categoryPermission) {
            return false;
        }

        return $categoryPermission->getAccessLevel();
    }

    public function availableCategoriesIds(string $categoryEntityFQN)
    {
        $tasksCategories = $this->entityManager
            ->getRepository($categoryEntityFQN)
            ->getFirstLevelCategories($this->getUser());

        /** @var CategoryInterface $category */
        foreach ($tasksCategories as $category) {
            $this->availableIds[] = $category->getId();
            if (!empty($category->getChildren())) {
                $this->fillArray($category);
            }
        }

        return $this->availableIds;
    }

    /**
     * @param CategoryInterface $category
     */
    private function fillArray(CategoryInterface $category)
    {
        switch ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            case true:
                foreach ($category->getChildren() as $child) {
                    array_push($this->availableIds, $child->getId());
                    $this->fillArray($child);
                }
                break;
            case false:
                if (!$category->isHidden()) {
                    foreach ($category->getChildren() as $child) {
                        if (!$child->isHidden()) {
                            array_push($this->availableIds, $child->getId());
                        }
                        $this->fillArray($child);
                    }
                }
                break;
        }
    }
}