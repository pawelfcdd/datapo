<?php

namespace App\Controller\Task;

use App\Controller\AppController;
use App\Entity\Task;
use App\Entity\TaskCategory;
use App\Entity\TaskCategoryPermission;
use App\Entity\TaskSectionPermission;
use App\Form\Task\TaskCategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class TaskCategoryController extends AppController
{
    /** @var string */
    public $sessionSelectedTaskCategoryName;
    /** @var int */
    public $sessionSelectedTaskCategoryId;
    public $breadcrumbsArray;


    public const LIMIT = 10;
    public const OFFSET = 10;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        parent::__construct($entityManager, $session);
        $this->sessionSelectedTaskCategoryName = $this->session->get(self::SELECTED_CATEGORY_NAME);
        $this->sessionSelectedTaskCategoryId = $this->session->get(self::SELECTED_CATEGORY_ID);
    }

    /**
     * @Route("/tasks/category/{id}/view", name="app.view_task_category", methods={"GET"})
     *
     * @param TaskCategory $taskCategory
     *
     * @return Response
     */
    public function viewCategoryPageAction(TaskCategory $taskCategory)
    {
        $access = $this->checkCategoryAccess(
            $this->getUser(),
            TaskCategoryPermission::class,
            $taskCategory
        );

        if (!$access) {
            return $this->render('layouts/no_access_section.html.twig');
        }

        if (!$this->session->isStarted()) {
            $this->session->start();
        } else {
            $this->session->set(self::SELECTED_CATEGORY_ID, $taskCategory->getId());
            $this->session->set(self::SELECTED_CATEGORY_NAME, $taskCategory->getName());
        }

        $lastTasks = $this->entityManager->getRepository(Task::class)
            ->getLastAdded($this->getUser(), self::LIMIT, [], $taskCategory, null);

        $taskCategories = $this->entityManager->getRepository(TaskCategory::class)
            ->getChildrenCategories($taskCategory->getId(), $this->getUser());

        $this->breadcrumbsAction($taskCategory, []);

        return $this->render('layouts/task/task_category_view.html.twig', [
            'taskCategory' => $taskCategory,
            'categories' => $taskCategories,
            'lastTasks' => $lastTasks,
            'access' => $access,
            'offset' => self::OFFSET,
            'limit' => self::LIMIT,
            'tableId' => 'task-paginator-table',
            'breadcrumbs' => $this->breadcrumbsArray
        ]);
    }

    public function breadcrumbsAction($category, $tree)
    {

        if ($category->getParent()) {
            $tree[$category->getParent()->getId()] = $category->getParent()->getName();
            $this->breadcrumbsAction($category->getParent(), $tree);
        }

        if (!$category->getParent()) {
            $this->breadcrumbsArray = array_reverse(array_flip($tree));
        }
    }


    /**
     * @Route("/tasks/category/add", name="app.add_task_category_form", methods={"GET"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function addFormRenderAction()
    {
        $form = $this->createForm(TaskCategoryType::class, null, [
            'action' => $this->generateUrl('app.add_task_category'),
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedTaskCategoryId,
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedTaskCategoryName,
        ]);
        
        return $this->render('layouts/task/task_category_form.html.twig', [
            'form' => $form->createView(),
            'page_header_text' => 'Добавить категорию',
            'category' => null
        ]);
    }

    /**
     * @Route("/tasks/category/{id}/edit", name="app.edit_task_category_form")
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param TaskCategory $taskCategory
     *
     * @return Response
     */
    public function editFormAction(TaskCategory $taskCategory)
    {
        $form = $this->createForm(TaskCategoryType::class, $taskCategory, [
            'SELECTED_CATEGORY_ID' => $taskCategory->getParent() ? $taskCategory->getParent()->getId() : null,
            'SELECTED_CATEGORY_NAME' => $taskCategory->getParent() ? $taskCategory->getParent()->getName() : null,
            'action' => $this->generateUrl(
                'app.update_task_category', ['id' => $taskCategory->getId()]
            ),
        ]);

        return $this->render('layouts/task/task_category_form.html.twig', [
            'form' => $form->createView(),
            'page_header_text' => 'Редактировать категорию',
            'selectedCategoryId' => $taskCategory->getId(),
            'category' => $taskCategory
        ]);
    }

    /**
     * @Route(
     *     "/tasks/category/add-task-category",
     *     name="app.add_task_category",
     *     methods={"POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function addTaskCategoryAction(Request $request)
    {
        $form = $this->createForm(TaskCategoryType::class, null, [
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedTaskCategoryId,
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedTaskCategoryName,
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var TaskCategory $formData */
            $formData = $form->getData();
            $accessLevel = 1;

            $parent = $this->entityManager->getRepository(TaskCategory::class)
                ->findOneById($this->sessionSelectedTaskCategoryId);

            $formData->setParent($parent);

            $this->getDoctrine()->getManager()->persist($formData);
            $this->getDoctrine()->getManager()->flush();


            $this->setNewCategoryPermission(new TaskCategoryPermission(), $formData);

            return $this->redirectToRoute('app.view_task_category', ['id' => $this->sessionSelectedTaskCategoryId ? $this->sessionSelectedTaskCategoryId : $formData->getId()]);
        }

        throw new \Exception('Can not save form');
    }

    /**
     * @Route(
     *     "/tasks/category/update-task-category/{id}",
     *      name="app.update_task_category"
     *     )
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param TaskCategory $category
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function editFormHandler(TaskCategory $category, Request $request)
    {
        $form = $this->createForm(TaskCategoryType::class, $category, [
            'SELECTED_CATEGORY_ID' => $category->getId(),
            'SELECTED_CATEGORY_NAME' => $category->getName(),
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var TaskCategory $formData */
            $formData = $form->getData();
            $parent = $this->entityManager->getRepository(TaskCategory::class)->findOneById($formData->getParent());
            $formData->setParent($parent);

            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app.view_task_category', ['id' => $category->getId()]);
    }

    /**
     * @Route("/tasks/category/{id}/remove", name="app.remove_task_category")
     *
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param TaskCategory $category
     *
     * @return RedirectResponse
     */
    public function removeAction(TaskCategory $category)
    {
        $this->entityManager->remove($category);
        $this->entityManager->flush();
        return $this->redirectToRoute('app.tasks');
    }

    /**
     * @Route("/tasks/category/{id}/show-next-tasks", name="app.show_next_tasks", methods={"POST"})
     *
     * @param TaskCategory $category
     * @param Request $request
     * @return Response
     */
    public function showNextPageAction(TaskCategory $category, Request $request)
    {
        $limit = intval($request->request->get('limit'));
        $offset = intval($request->request->get('offset'));

        if ($offset === 0) {
            $offset = $offset + self::OFFSET;
        }

        $tasks = $this->entityManager->getRepository(Task::class)
            ->getLastAdded($this->getUser(), $limit, 'new', $category, $offset);

        $offset = $offset + self::OFFSET;

        return $this->render('partials/ajax/paginate_table_body.html.twig', [
            'offset' => $offset,
            'limit' => $limit,
            'contentItems' => $tasks,
            'link_to_content_item' => $this->generateUrl('app.view_task', ['id' => 'id']),
        ]);
    }

    /**
     * @Route("/tasks/category/{id}/show-prev-tasks", name="app.show_prev_tasks", methods={"POST"})
     *
     * @param TaskCategory $category
     * @param Request $request
     * @return Response
     */
    public function showPrevPageAction(TaskCategory $category, Request $request)
    {
        $limit = $request->request->get('limit');
        $offset = $request->request->get('offset');
        $offset = $offset - self::OFFSET * 2;

        if ($offset < 0) {
            $offset = 0;
        }

        $tasks = $this->entityManager->getRepository(Task::class)
            ->getLastAdded($this->getUser(), $limit, 'new', $category, $offset);

        return $this->render('partials/ajax/paginate_table_body.html.twig', [
            'offset' => $offset,
            'limit' => $limit,
            'contentItems' => $tasks,
            'link_to_content_item' => $this->generateUrl('app.view_task', ['id' => 'id']),
        ]);
    }
}