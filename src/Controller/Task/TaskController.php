<?php

namespace App\Controller\Task;

use App\Controller\AppController;
use App\Entity\Interfaces\CategoryInterface;
use App\Entity\Task;
use App\Entity\TaskCategory;
use App\Entity\TaskCategoryPermission;
use App\Entity\TaskComment;
use App\Entity\TaskSectionPermission;
use App\Entity\Upload;
use App\Entity\UploadCategoryPermission;
use App\Form\Task\TaskType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AppController
{
    /** @var string */
    public $sessionSelectedTaskCategoryName;
    public $sessionSelectedTaskCategoryId;
    public $breadcrumbsArray;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        parent::__construct($entityManager, $session);
        $this->sessionSelectedTaskCategoryName = $this->session->get(self::SELECTED_CATEGORY_NAME);
        $this->sessionSelectedTaskCategoryId = $this->session->get(self::SELECTED_CATEGORY_ID);
    }

    /**
     * @Route("/tasks", name="app.tasks")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPageAction()
    {
        $access = $this->checkSectionAccess($this->getUser(), TaskSectionPermission::class);

        if (!$access) {
            return $this->render('layouts/no_access_section.html.twig');
        }
        $tasksCategories = $this->entityManager
            ->getRepository(TaskCategory::class)
            ->getFirstLevelCategories($this->getUser());
        $lastTasks = [];

        if (!empty($tasksCategories)) {
            $categories = $this->availableCategoriesIds(TaskCategory::class);

            $lastTasks = $this->entityManager
                ->getRepository(Task::class)
                ->getLastAdded($this->getUser(), 10, [], null, null, $categories);
        }

        $this->session->remove(self::SELECTED_CATEGORY_NAME);
        $this->session->remove(self::SELECTED_CATEGORY_ID);

        return $this->render('layouts/task/index.html.twig', [
            'tasksCategories' => $tasksCategories,
            'lastTasks' => $lastTasks,
            'access' => $access
        ]);
    }

    /**
     * @Route("/tasks/task/add", name="app.add_task_form_action", methods={"GET"})
     */
    public function addTaskFormAction()
    {
        $form = $this->createForm(TaskType::class, null, [
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedTaskCategoryName,
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedTaskCategoryId,
            'USER' => $this->getUser(),
            'action' => $this->generateUrl('app.save_new_task')
        ]);

        $category = $this->entityManager->getRepository(TaskCategory::class)
            ->findOneById($this->sessionSelectedTaskCategoryId);

        return $this->render('layouts/task/task_form.html.twig', [
            'form' => $form->createView(),
            'category' => $category,
            'selectedCategoryId' => $this->sessionSelectedTaskCategoryId,
            'breadcrumbs' => $this->generateBreadcrumbs($category)

        ]);

    }

    /**
     * @Route("/tasks/task/save-new-task", name="app.save_new_task", methods={"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveNewTaskAction(Request $request)
    {
        $form = $this->createForm(TaskType::class, null, [
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedTaskCategoryName,
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedTaskCategoryId,
            'USER' => $this->getUser()
        ])->handleRequest($request);

        $attachments = $request->request->get('task')['attachments'];

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Task $formData */
            $formData = $form->getData();
            $taskCategory = $this->entityManager
                ->getRepository(TaskCategory::class)
                ->findOneById($this->sessionSelectedTaskCategoryId);

            $formData
                ->setCategory($taskCategory)
                ->setStatus('new')
                ->setAuthor($this->getUser());

            if (!empty($attachments)) {
                $attachmentsArray = explode(',', $attachments);

                foreach ($attachmentsArray as $attachmentId) {
                    $upload = $this->entityManager
                        ->getRepository(Upload::class)
                        ->findOneById(trim($attachmentId));

                    if ($upload instanceof Upload) {
                        $formData->addAttachment($upload);
                    }
                }
            }

            if ($this->getUser()->hasRole('ROLE_EDITOR')) {
                $formData->setHidden(false);
            }

            $this->entityManager->persist($formData);
            $this->entityManager->flush();

        }

        return $this->redirectToRoute('app.view_task', ['id' => $formData->getId()]);
    }

    /**
     * @Route("/tasks/task/{id}/view", name="app.view_task")
     *
     * @param Task $task
     *
     * @return Response
     */
    public function viewTaskAction(Task $task)
    {
        $taskAttachments = $task->getAttachments();
        $accessLevel = $this->checkCategoryAccess($this->getUser(), TaskCategoryPermission::class, $task->getCategory());
        $allowedAttachments = [];

        /** @var Upload $taskAttachment */
        foreach ($taskAttachments as $taskAttachment) {
            /** @var UploadCategoryPermission $permission */
            $permission = $this->entityManager->getRepository(UploadCategoryPermission::class)
                ->findOneBy([
                    'category' => $taskAttachment->getCategory()->getId(),
                    'user' => $this->getUser()->getId()
                ]);

            if ($permission->getAccessLevel()) {
                if ($taskAttachment->isHidden()) {
                    if($this->getUser()->hasRole('ROLE_SUPER_ADMIN') ) {
                        array_push($allowedAttachments, $taskAttachment);
                    }
                } else {
                    array_push($allowedAttachments, $taskAttachment);
                }
            }
        }

        $this->breadcrumbsAction($task->getCategory(), []);

        return $this->render('layouts/task/view_task.html.twig', [
            'task' => $task,
            'allowedAttachments' => $allowedAttachments,
            'breadcrumbs' => $this->breadcrumbsArray,
            'accessLevel' => $accessLevel
        ]);
    }

    /**
     * @Route("/tasks/task/{id}/edit", name="app.edit_task_form")
     *
     * @param Task $task
     *
     * @return Response
     */
    public function  editFormAction(Task $task)
    {
        $form = $this->createForm(TaskType::class, $task, [
            'SELECTED_CATEGORY_NAME' => null,
            'SELECTED_CATEGORY_ID' => null,
            'USER' => $this->getUser(),
            'action' => $this->generateUrl('app.update_task_action', [
                'id' => $task->getId()
            ])
        ]);

        $taskAttachments = $task->getAttachments();
        $allowedAttachments = [];

        /** @var Upload $taskAttachment */
        foreach ($taskAttachments as $taskAttachment) {
            /** @var UploadCategoryPermission $permission */
            $permission = $this->entityManager->getRepository(UploadCategoryPermission::class)
                ->findOneBy([
                    'category' => $taskAttachment->getCategory()->getId(),
                    'user' => $this->getUser()->getId()
                ]);

            if ($permission->getAccessLevel()) {
                array_push($allowedAttachments, $taskAttachment);
            }
        }

        return $this->render('layouts/task/edit_task_form.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
            'allowedAttachments' => $allowedAttachments,
            'breadcrumbs' => $this->generateBreadcrumbs($task->getCategory())
        ]);
    }

    /**
     * @Route("/tasks/task/{id}/remove", name="app.remove_task_action", methods={"POST"})
     *
     * @param Task $task
     *
     * @return RedirectResponse
     *
     */
    public function deleteTaskAction(Task $task)
    {
        $this->entityManager->remove($task);
        $this->entityManager->flush();

        return $this->redirectToRoute('app.view_task_category', ['id' => $task->getCategory()->getId()]);
    }

    /**
     * @Route("/tasks/task/{id}/update-task", name="app.update_task_action")
     *
     * @param Task $task
     * @param Request $request
     *
     * @return RedirectResponse
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateTaskAction(Task $task, Request $request)
    {
        $form = $this->createForm(TaskType::class, $task, [
            'SELECTED_CATEGORY_ID' => null,
            'SELECTED_CATEGORY_NAME' => null,
            'USER' => $this->getUser(),
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $attachments = $request->request->get('task')['attachments'];

            if (!empty($attachments)) {
                $attachmentsArray = explode(',', $attachments);

                foreach ($attachmentsArray as $key => $value) {
                    $attachmentsArray[$key] = intval($value);
                }

                $knowledgeAttachmentsArray = [];

                foreach ($task->getAttachments() as $attachment) {
                    $knowledgeAttachmentsArray[] = $attachment->getId();
                }

                if (count($attachmentsArray) < count($knowledgeAttachmentsArray)) {
                    $attachmentsToRemove = array_diff($knowledgeAttachmentsArray, $attachmentsArray);

                    foreach ($attachmentsToRemove as $key=>$val) {
                        $upload = $this->entityManager
                            ->getRepository(Upload::class)
                            ->findOneById(trim($val));

                        $task->removeAttachment($upload);
                    }

                }

                foreach ($attachmentsArray as $attachmentId) {
                    $upload = $this->entityManager
                        ->getRepository(Upload::class)
                        ->findOneById(trim($attachmentId));

                    if ($upload instanceof Upload) {
                        $formData->addAttachment($upload);
                    }
                }
            } else {
                foreach ($task->getAttachments() as $attachment) {
                    $task->removeAttachment($attachment);
                }
            }

            $this->entityManager->flush();

            return $this->redirectToRoute('app.view_task', ['id' => $task->getId()]);

        }
    }

    /**
     * @Route("/tasks/filter-tasks", name="app.filter-tasks", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function taskFilterAjaxAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $filterParams = $request->request->get('filterParams');
            $parentCategoryId = $request->request->get('parentCategory');

            $category = $this->entityManager->getRepository(TaskCategory::class)->findOneById($parentCategoryId);

            $filteredData = $this->entityManager->getRepository(Task::class)
                ->getLastAdded($this->getUser(), 10, $filterParams, $category);

            return $this->render('partials/ajax/task_filtering.html.twig', [
                'contentItems' => $filteredData,
                'link_to_content_item' => $this->generateUrl('app.view_task', ['id'=> 'id']),
            ]);
        }
    }

    /**
     * @Route("/tasks/task/{id}/add-comment", name="app.task_add_comment", methods={"POST"})
     *
     * @param Task $task
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function addCommentAction(Task $task, Request $request)
    {
        $taskComment = new TaskComment();
        $userComment = $request->request->get('user-comment');

        if ($task->getAuthor()->getId() == $this->getUser()->getId()) {
            $taskComment->setIsAuthor(true);
        } else {
            $taskComment->setIsAuthor(false);
        }

        $taskComment
            ->setUser($this->getUser())
            ->setTask($task)
            ->setUserComment($userComment);

        $this->entityManager->persist($taskComment);
        $this->entityManager->flush();

        return $this->redirectToRoute('app.view_task', ['id' => $task->getId()]);
    }

    public function breadcrumbsAction($category, $tree)
    {

        if ($category->getParent()) {
            $tree[$category->getParent()->getId()] = $category->getParent()->getName();
            $this->breadcrumbsAction($category->getParent(), $tree);
        }

        if (!$category->getParent()) {
            $this->breadcrumbsArray = array_reverse(array_flip($tree));
        }
    }
}