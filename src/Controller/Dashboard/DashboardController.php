<?php

namespace App\Controller\Dashboard;

use App\Controller\AppController;
use App\Entity\Knowledge;
use App\Entity\KnowledgeCategory;
use App\Entity\Task;
use App\Entity\TaskCategory;
use App\Entity\Upload;
use App\Entity\UploadCategory;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AppController
{
    /**
     * @Route("/", name="app.dashboard", methods={"GET"})
     */
    public function renderPageAction() {

        $lastTasks = $this->entityManager
            ->getRepository(Task::class)
            ->getLastAdded($this->getUser(), 10, 'new', null, null, $this->availableCategoriesIds(TaskCategory::class));

        $lastKnowledge = $this->entityManager
            ->getRepository(Knowledge::class)
            ->getLastAdded($this->getUser(), 10, null, null, $this->availableCategoriesIds(KnowledgeCategory::class));

        $lastUploads = $this->entityManager
            ->getRepository(Upload::class)
            ->getLastAdded($this->getUser(), 10, null, $this->availableCategoriesIds(UploadCategory::class));

        return $this->render('layouts/dashboard/index.html.twig', [
            'page_title' => 'Главная',
            'lastTasks' => $lastTasks,
            'lastKnowledge' => $lastKnowledge,
            'lastUploads' => $lastUploads
        ]);
    }
}