<?php

namespace App\Controller\Uploads;

use App\Controller\AppController;
use App\Entity\Upload;
use App\Entity\UploadCategory;
use App\Entity\UploadsSectionPermission;
use App\Form\Upload\UploadType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AppController
{
    public $kernel;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, KernelInterface $kernel)
    {
        parent::__construct($entityManager, $session);
        $this->kernel = $kernel;
    }

    /**
     * @Route("/uploads", name="app.uploads_index")
     */
    public function indexAction()
    {

        $this->session->remove(self::SELECTED_CATEGORY_NAME);
        $this->session->remove(self::SELECTED_CATEGORY_ID);

        $access = $this->checkSectionAccess($this->getUser(), UploadsSectionPermission::class);

        if (!$access) {
            return $this->render('layouts/no_access_section.html.twig');
        }

        $uploadCategories = $this->entityManager
            ->getRepository(UploadCategory::class)->getFirstLevelCategories($this->getUser());

        $uploadFiles = $this->entityManager->getRepository(Upload::class)
            ->getLastAdded($this->getUser(), 10, null, $this->availableCategoriesIds(UploadCategory::class));

        return $this->render('layouts/uploads/index.html.twig', [
            'uploadCategories' => $uploadCategories,
            'uploadFiles' => $uploadFiles,
            'access' => $access
        ]);
    }

    /**
     * @Route("/uploads/file/add", name="app.uploads_add_file")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addFileFormAction(Request $request)
    {
        $form = $this->createForm(UploadType::class, null, [
            'USER' => $this->getUser()
        ])->handleRequest($request);
        /** @var UploadCategory $uploadCategory */
        $uploadCategory = $this->entityManager->getRepository(UploadCategory::class)->findOneById($this->session->get(self::SELECTED_CATEGORY_ID));

        if ($form->isSubmitted() && $form->isValid())
        {
            /** @var Upload $uploadData */
            $uploadData = $form->getData();
            /** @var UploadedFile $file */
            $file = $uploadData->getFile();
           /** @var Upload $upload */
            $upload = new Upload();

            $upload
                ->setCategory($uploadCategory)
                ->setName($uploadData->getName())
                ->setFullFileName($uploadData->getName() . '.' . $file->guessClientExtension())
                ->setDescription($uploadData->getDescription())
                ->setHidden($this->getUser()->hasRole('ROLE_SUPER_ADMIN') ? $uploadData->isHidden() : false)
                ->setAuthor($this->getUser())
                ->setExtension($file->guessClientExtension());

            try {
                $this->entityManager->persist($upload);
                $this->entityManager->flush();
                $this->uploadFile($file, $upload);
                return $this->redirectToRoute('app.view_upload_category', [
                    'id' => $upload->getCategory()->getId(),
                ]);
            } catch (\Exception $exception) {

            }
        }

        return $this->render('layouts/uploads/upload_file_form.html.twig', [
            'form' => $form->createView(),
            'category' => $uploadCategory
        ]);
    }

    public function uploadFile(UploadedFile $file, Upload $upload)
    {
        $fileName = $upload->getId() . '.' . $file->guessClientExtension();
        $folderName = date('Y/m/d');
        $projectDir = $this->getParameter('kernel.project_dir');
        $uploadFilesDir = $this->getParameter('uploads_dir');
        $pathToUpload = $projectDir . $uploadFilesDir .$folderName;
        $fullPathToFile = "/upload_files/$folderName/$fileName";

        if (!is_dir($pathToUpload)) {
            mkdir($pathToUpload, 0777, true);
        }

        try {
            $file->move($pathToUpload, $fileName);
            $upload->setFile($fullPathToFile);
            $this->entityManager->flush();
        } catch (FileException $exception) {}
    }

    /**
     * @Route("/uploads/file/{id}/delete", name="app.uploads_delete_file", methods={"POST"})
     *
     * @param Upload $upload
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteFile(Upload $upload)
    {
        $filesystem = new Filesystem();
        $publicDir = $this->kernel->getProjectDir() .'/public';
        $filePath = $publicDir . $upload->getFile();

        $this->entityManager->remove($upload);
        $filesystem->remove($filePath);

        $this->entityManager->flush();

        return $this->redirectToRoute('app.view_upload_category', [
            'id' => $upload->getCategory()->getId()
        ]);
    }
}