<?php

namespace App\Controller\Uploads;

use App\Controller\AppController;
use App\Entity\Upload;
use App\Entity\UploadCategory;
use App\Entity\UploadCategoryPermission;
use App\Form\Upload\UploadCategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class UploadCategoryController extends AppController
{
    /** @var string */
    private $sessionSelectedCategoryName;
    /** @var int  */
    private $sessionSelectedCategoryId;

    public $breadcrumbsArray;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        parent::__construct($entityManager, $session);

        $this->sessionSelectedCategoryName = $this->session->get(self::SELECTED_CATEGORY_NAME);
        $this->sessionSelectedCategoryId = $this->session->get(self::SELECTED_CATEGORY_ID);
    }

    /**
     * @Route("/uploads/category/add", name="app.add_upload_category_form")
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFormRenderAction()
    {
        $form = $this->createForm(UploadCategoryType::class, null, [
            'action' => $this->generateUrl('app.add_upload_category'),
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedCategoryId,
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedCategoryName,
        ]);

        return $this->render('layouts/uploads/upload_category_form.html.twig', [
            'form' => $form->createView(),
            'page_header_text' => 'Добавить категорию',
            'return_link' => $this->generateUrl('app.uploads_index'),
            'selectedCategoryId' => $this->sessionSelectedCategoryId,
        ]);
    }

    /**
     * @Route(
     *     "/uploads/category/add-upload-category",
     *     name="app.add_upload_category",
     *     methods={"POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function saveCategoryAction(Request $request)
    {
        $form = $this->createForm(UploadCategoryType::class, null, [
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedCategoryId,
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedCategoryName,
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadCategory $formData */
            $formData = $form->getData();
            $accessLevel = 1;

            $parent = $this->entityManager->getRepository(UploadCategory::class)
                ->findOneById($this->sessionSelectedCategoryId);

            $formData->setParent($parent);

            $this->getDoctrine()->getManager()->persist($formData);
            $this->getDoctrine()->getManager()->flush();

            $this->setNewCategoryPermission(new UploadCategoryPermission(), $formData);

            return $this->redirectToRoute('app.view_upload_category', ['id' => $formData->getId()]);
        }

        throw new \Exception('Can not save form');
    }

    /**
     * @Route("/uploads/category/{id}/view", name="app.view_upload_category", methods={"GET"})
     *
     * @param UploadCategory $uploadCategory
     *
     * @return Response
     */
    public function viewCategoryPageAction(UploadCategory $uploadCategory)
    {
        $access = $this->checkCategoryAccess($this->getUser(), UploadCategoryPermission::class, $uploadCategory
        );

        if (!$access) {
            return $this->render('layouts/no_access_section.html.twig');
        }

        if (!$this->session->isStarted()) {
            $this->session->start();
        } else {
            $this->session->set(self::SELECTED_CATEGORY_ID, $uploadCategory->getId());
            $this->session->set(self::SELECTED_CATEGORY_NAME, $uploadCategory->getName());
        }

        $this->breadcrumbsAction($uploadCategory, []);

        $attachments = [];

        foreach ($uploadCategory->getUploads() as $upload)
        {
            if ($upload->isHidden()) {
                if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
                    $attachments[] = $upload;
                }
            } else {
                $attachments[] = $upload;
            }
        }

        $categories = $this->entityManager->getRepository(UploadCategory::class)->getChildrenCategories($uploadCategory->getId(), $this->getUser());

        $categoryFiles = $this->entityManager
            ->getRepository(Upload::class)
            ->getLastAdded($this->getUser(), 10, $uploadCategory);

        return $this->render('layouts/uploads/upload_category_view.html.twig', [
            'uploadCategory' => $uploadCategory,
            'categories' => $categories,
            'access' => $access,
            'breadcrumbs' => $this->breadcrumbsArray,
            'attachments' => $categoryFiles,
        ]);
    }

    public function getSubcategoryAttachment(UploadCategory $category, array $attachments = null)
    {
        /** @var UploadCategory $child */
        foreach ($category->getChildren() as $child)
        {
            if (!empty($child->getUploads())) {
                /** @var Upload $upload */
                foreach ($child->getUploads() as $upload) {
                    if ($upload->isHidden()) {
                        if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
                            $attachments[] = $upload;
                        }
                    } else {
                        $attachments[] = $upload;
                    }
                }
            }

            if (!empty($child->getChildren())) {
                $this->getSubcategoryAttachment($child, $attachments);
            }
        }

        return $attachments;
    }

    public function breadcrumbsAction($category, $tree)
    {

        if ($category->getParent()) {
            $tree[$category->getParent()->getId()] = $category->getParent()->getName();
            $this->breadcrumbsAction($category->getParent(), $tree);
        }

        if (!$category->getParent()) {
            $this->breadcrumbsArray = array_reverse(array_flip($tree));
        }
    }

    /**
     * @Route("/uploads/category/{id}/edit", name="app.edit_upload_category_form")
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param UploadCategory $uploadCategory
     *
     * @return Response
     */
    public function editFormAction(UploadCategory $uploadCategory)
    {
        $form = $this->createForm(UploadCategoryType::class, $uploadCategory, [
            'SELECTED_CATEGORY_ID' => $uploadCategory->getParent() ? $uploadCategory->getParent()->getId() : null,
            'SELECTED_CATEGORY_NAME' => $uploadCategory->getParent() ? $uploadCategory->getParent()->getName() : null,
            'action' => $this->generateUrl(
                'app.update_upload_category', ['id' => $uploadCategory->getId()]
            ),
        ]);

        return $this->render('layouts/uploads/upload_category_form.html.twig', [
            'form' => $form->createView(),
            'page_header_text' => 'Редактировать категорию',
            'selectedCategoryId' => $uploadCategory->getId(),
            'category' => $uploadCategory,
        ]);
    }

    /**
     * @Route(
     *     "/uploads/category/update-upload-category/{id}",
     *      name="app.update_upload_category"
     *     )
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param UploadCategory $category
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function editFormHandler(UploadCategory $category, Request $request)
    {
        $form = $this->createForm(UploadCategoryType::class, $category, [
            'SELECTED_CATEGORY_ID' => $category->getId(),
            'SELECTED_CATEGORY_NAME' => $category->getName(),
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadCategory $formData */
            $formData = $form->getData();

            $parent = $this->entityManager
                ->getRepository(UploadCategory::class)
                ->findOneById($formData->getParent());
            $formData->setParent($parent);

            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app.view_upload_category', ['id' => $category->getId()]);
    }

    /**
     * @Route("/uploads/category/{id}/remove", name="app.remove_upload_category")
     *
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param UploadCategory $category
     *
     * @return RedirectResponse
     */
    public function removeAction(UploadCategory $category)
    {
        $this->entityManager->remove($category);
        $this->entityManager->flush();
        return $this->redirectToRoute('app.uploads_index');
    }
}