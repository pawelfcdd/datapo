<?php

namespace App\Controller\Settings;

use App\Controller\AppController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

/**
 * Class SettingsController
 * @package App\Controller\Settings
 * @IsGranted("ROLE_SUPER_ADMIN")
 */
class SettingsController extends AppController
{

    /**
     * @Route("/settings", name="app.settings")
     */
    public function indexAction()
    {
        $backupFtpFile = Yaml::parseFile($this->getParameter('backups_ftp_file'));
        $backupPeriodicityFile = Yaml::parseFile($this->getParameter('backups_periodicity_file'));
        $filesAuthkey = Yaml::parseFile($this->getParameter('files_authkey'));

        return $this->render('layouts/settings/index.html.twig', [
            'filesAuthKey' => $filesAuthkey,
            'backupFtpFile' => $backupFtpFile,
            'backupPeriodicity' => $backupPeriodicityFile
        ]);
    }

    /**
     * @Route("/settings/save-files-auth-key", name="app.setting_save_file_auth_key",methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveFilesAuthKey(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = Yaml::dump($request->request->get('fileAuthKey'));
            file_put_contents($this->getParameter('files_authkey'), $data);
        }
        return $this->redirectToRoute('app.settings');
    }

    /**
     * @Route("/settings/save-backup-ftp-file", name="app.setting_save_backup_ftp_file", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveBackupFtpFile(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = Yaml::dump($request->request->get('backupFtpFile'));
            file_put_contents($this->getParameter('backups_ftp_file'), $data);
        }
        return $this->redirectToRoute('app.settings');
    }

    /**
     * @Route("/settings/save-backup-periodicity-file", name="app.setting_save_backup_periodicity_file", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveBackupPeriodicityFile(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = Yaml::dump($request->request->get('backupPeriodicity'));
            file_put_contents($this->getParameter('backups_periodicity_file'), $data);
        }
        return $this->redirectToRoute('app.settings');
    }
}