<?php

namespace App\Controller\Knowledge;

use App\Controller\AppController;
use App\Entity\Knowledge;
use App\Entity\KnowledgeCategory;
use App\Entity\KnowledgeCategoryPermission;
use App\Entity\KnowledgeSectionPermission;
use App\Entity\Upload;
use App\Entity\UploadCategoryPermission;
use App\Form\Knowledge\KnowledgeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class KnowledgeController extends AppController
{
    /** @var string */
    public $sessionSelectedKnowledgeCategoryName;
    public $sessionSelectedKnowledgeCategoryId;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        parent::__construct($entityManager, $session);

        $this->sessionSelectedKnowledgeCategoryId = $this->session->get(self::SELECTED_CATEGORY_ID);
        $this->sessionSelectedKnowledgeCategoryName = $this->session->get(self::SELECTED_CATEGORY_NAME);
    }

    /**
     * @Route("/knowledge", name="app.knowledge_index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPageAction()
    {
        $access = $this->checkSectionAccess($this->getUser(), KnowledgeSectionPermission::class);

        if (!$access) {
            return $this->render('layouts/no_access_section.html.twig');
        }

        $knowledgeCategories = $this->entityManager
            ->getRepository(KnowledgeCategory::class)
            ->getFirstLevelCategories($this->getUser());

        $lastKnowledgeAdded = [];

        if (!empty($knowledgeCategories)) {
            $lastKnowledgeAdded = $this->entityManager
                ->getRepository(Knowledge::class)
                ->getLastAdded($this->getUser(), 10, null, null, $this->availableCategoriesIds(KnowledgeCategory::class));
        }

        $this->session->remove(self::SELECTED_CATEGORY_NAME);
        $this->session->remove(self::SELECTED_CATEGORY_ID);

        return $this->render('layouts/knowledge/index.html.twig', [
            'knowledgeCategories' => $knowledgeCategories,
            'lastKnowledgeAdded' => $lastKnowledgeAdded,
            'access' => $access
        ]);
    }

    /**
     * @Route("/knowledge/article/add", name="app.add_article_form_action", methods={"GET"})
     */
    public function addNewArticleFormAction()
    {
        $form = $this
            ->createForm(KnowledgeType::class, null, [
                'SELECTED_CATEGORY_NAME' => $this->sessionSelectedKnowledgeCategoryName,
                'SELECTED_CATEGORY_ID' => $this->sessionSelectedKnowledgeCategoryId,
                'USER' => $this->getUser(),
                'action' => $this->generateUrl('app.save_article_action')
            ]);

        $category = $this->entityManager->getRepository(KnowledgeCategory::class)
            ->findOneById($this->sessionSelectedKnowledgeCategoryId);

        return $this->render('layouts/knowledge/knowledge_form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Добавить статью',
            'category' => $category,
            'breadcrumbs' => $this->generateBreadcrumbs($category)
        ]);
    }

    /**
     * @Route("/knowledge/article/save-action", name="app.save_article_action", methods={"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveNewArticleAction(Request $request)
    {
        $form = $this
            ->createForm(KnowledgeType::class, null, [
                'SELECTED_CATEGORY_NAME' => $this->sessionSelectedKnowledgeCategoryName,
                'SELECTED_CATEGORY_ID' => $this->sessionSelectedKnowledgeCategoryId,
                'USER' => $this->getUser(),
            ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Knowledge $formData */
            $formData = $form->getData();
            $attachments = $request->request->get('knowledge')['attachments'];

            $taskCategory = $this->entityManager
                ->getRepository(KnowledgeCategory::class)
                ->findOneById($this->sessionSelectedKnowledgeCategoryId);

            $formData
                ->setCategory($taskCategory)
                ->setAuthor($this->getUser());

            if (!empty($attachments))
            {
                $attachmentsArray = explode(',', $attachments);

                foreach ($attachmentsArray as $attachmentId) {
                    $upload = $this->entityManager
                        ->getRepository(Upload::class)
                        ->findOneById(trim($attachmentId));

                    if ($upload instanceof Upload) {
                        $formData->addAttachment($upload);
                    }
                }
            }

            if ($this->getUser()->hasRole('ROLE_EDITOR')) {
                $formData->setHidden(false);
            }

            $this->entityManager->persist($formData);
            $this->entityManager->flush();
            $redirectParameter = $formData->getCategory()->getId();
        }

        return $this->redirectToRoute('app.view_knowledge_category', [
            'id' => $redirectParameter
        ]);
    }

    /**
     * @Route("/knowledge/article/{id}/view", name="app.view_knowledge")
     *
     * @param Knowledge $knowledge
     *
     * @return Response
     */
    public function viewArticleAction(Knowledge $knowledge)
    {
        $taskAttachments = $knowledge->getAttachments();
        $accessLevel = $this->checkCategoryAccess($this->getUser(), KnowledgeCategoryPermission::class, $knowledge->getCategory());
        $allowedAttachments = [];

        /** @var Upload $taskAttachment */
        foreach ($taskAttachments as $taskAttachment) {
            /** @var UploadCategoryPermission $permission */
            $permission = $this->entityManager->getRepository(UploadCategoryPermission::class)
                ->findOneBy([
                    'category' => $taskAttachment->getCategory()->getId(),
                    'user' => $this->getUser()->getId()
                ]);

            if ($permission->getAccessLevel()) {
                if ($taskAttachment->isHidden()) {
                    if($this->getUser()->hasRole('ROLE_SUPER_ADMIN') ) {
                        array_push($allowedAttachments, $taskAttachment);
                    }
                } else {
                    array_push($allowedAttachments, $taskAttachment);
                }
            }
        }

        return $this->render('layouts/knowledge/knowledge_view.html.twig', [
            'knowledge' => $knowledge,
            'category' => $knowledge->getCategory(),
            'allowedAttachments' => $allowedAttachments,
            'breadcrumbs' => $this->generateBreadcrumbs($knowledge->getCategory()),
            'accessLevel' => $accessLevel,
        ]);
    }

    /**
     * @Route("/knowledge/article/{id}/edit", name="app.edit_knowledge_article_form")
     *
     * @param Knowledge $knowledge
     *
     * @return Response
     */
    public function  editFormAction(Knowledge $knowledge)
    {
        $allowedAttachments = [];
        $taskAttachments = $knowledge->getAttachments();

        $form = $this->createForm(KnowledgeType::class, $knowledge, [
            'SELECTED_CATEGORY_NAME' => null,
            'SELECTED_CATEGORY_ID' => null,
            'USER' => $this->getUser(),
            'action' => $this->generateUrl('app.update_knowledge_article_action', [
                'id' => $knowledge->getId()
            ])
        ]);

        /** @var Upload $taskAttachment */
        foreach ($taskAttachments as $taskAttachment) {
            /** @var UploadCategoryPermission $permission */
            $permission = $this->entityManager->getRepository(UploadCategoryPermission::class)
                ->findOneBy([
                    'category' => $taskAttachment->getCategory()->getId(),
                    'user' => $this->getUser()->getId()
                ]);

            if ($permission->getAccessLevel()) {
                if ($taskAttachment->isHidden()) {
                    if($this->getUser()->hasRole('ROLE_SUPER_ADMIN') ) {
                        array_push($allowedAttachments, $taskAttachment);
                    }
                } else {
                    array_push($allowedAttachments, $taskAttachment);
                }
            }
        }

        return $this->render('layouts/knowledge/knowledge_form.html.twig', [
            'form' => $form->createView(),
            'category' => $knowledge->getCategory(),
            'knowledge' => $knowledge,
            'title' => 'Редактировать статью ' . $knowledge->getName(),
            'breadcrumbs' => $this->generateBreadcrumbs($knowledge->getCategory()),
            'allowedAttachments' => $allowedAttachments,

        ]);
    }

    /**
     * @Route("/knowledge/article/{id}/update-article", name="app.update_knowledge_article_action", methods={"POST"})
     *
     * @param Knowledge $knowledge
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateKnowledgeArticleAction(Knowledge $knowledge, Request $request)
    {
        $form = $this->createForm(KnowledgeType::class, $knowledge, [
            'SELECTED_CATEGORY_ID' => null,
            'SELECTED_CATEGORY_NAME' => null,
            'USER' => $this->getUser(),
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $attachments = $request->request->get('knowledge')['attachments'];

            if (!empty($attachments)) {
                $attachmentsArray = explode(',', $attachments);

                foreach ($attachmentsArray as $key => $value) {
                    $attachmentsArray[$key] = intval($value);
                }

                $knowledgeAttachmentsArray = [];

                foreach ($knowledge->getAttachments() as $attachment) {
                    $knowledgeAttachmentsArray[] = $attachment->getId();
                }

                if (count($attachmentsArray) < count($knowledgeAttachmentsArray)) {
                    $attachmentsToRemove = array_diff($knowledgeAttachmentsArray, $attachmentsArray);

                    foreach ($attachmentsToRemove as $key=>$val) {
                       $upload = $this->entityManager
                           ->getRepository(Upload::class)
                           ->findOneById(trim($val));

                       $knowledge->removeAttachment($upload);
                   }

                }

                foreach ($attachmentsArray as $attachmentId) {
                    $upload = $this->entityManager
                        ->getRepository(Upload::class)
                        ->findOneById(trim($attachmentId));

                    if ($upload instanceof Upload) {
                        $formData->addAttachment($upload);
                    }
                }
            } else {
                foreach ($knowledge->getAttachments() as $attachment) {
                    $knowledge->removeAttachment($attachment);
                }
            }

            $this->entityManager->flush();

            return $this->redirectToRoute('app.view_knowledge', ['id' => $knowledge->getId()]);

        }
    }

    /**
     * @Route("/knowledge/article/{id}/remove", name="app.remove_article_action", methods={"POST"})
     *
     * @param Knowledge $knowledge
     *
     * @return RedirectResponse
     *
     */
    public function deleteTaskAction(Knowledge $knowledge)
    {
        $this->entityManager->remove($knowledge);
        $this->entityManager->flush();

        return $this->redirectToRoute('app.view_knowledge_category', ['id' => $knowledge->getCategory()->getId()]);
    }

}