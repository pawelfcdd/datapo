<?php

namespace App\Controller\Knowledge;

use App\Controller\AppController;
use App\Entity\Knowledge;
use App\Entity\KnowledgeCategory;
use App\Entity\KnowledgeCategoryPermission;
use App\Entity\TaskCategoryPermission;
use App\Form\Knowledge\KnowledgeCategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class KnowledgeCategoryController extends AppController
{
    /** @var string */
    public $sessionSelectedKnowledgeCategoryName;
    /** @var int  */
    public $sessionSelectedKnowledgeCategoryId;

    public $breadcrumbsArray;

    public const LIMIT = 10;
    public const OFFSET = 10;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        parent::__construct($entityManager, $session);
        $this->sessionSelectedKnowledgeCategoryName = $this->session->get(self::SELECTED_CATEGORY_NAME);
        $this->sessionSelectedKnowledgeCategoryId = $this->session->get(self::SELECTED_CATEGORY_ID);
    }

    /**
     * @Route("/knowledge/category/add", name="app.add_knowledge_category_form")
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFormRenderAction()
    {
        $form = $this->createForm(KnowledgeCategoryType::class, null, [
            'action' => $this->generateUrl('app.add_new_knowledge_category'),
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedKnowledgeCategoryId,
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedKnowledgeCategoryName,
        ]);

        $category = $this->entityManager
            ->getRepository(KnowledgeCategory::class)
            ->findOneById($this->sessionSelectedKnowledgeCategoryId);

        return $this->render('layouts/knowledge/knowledge_category_form.html.twig', [
            'form' => $form->createView(),
            'page_header_text' => 'Добавить категорию',
            'return_link' => $this->generateUrl('app.knowledge_index'),
            'deleteButton' => false,
        ]);
    }

    /**
     * @Route("/knowledge/category/save-action", name="app.add_new_knowledge_category", methods={"POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function addKnowledgeCategoryAction(Request $request)
    {
        $form = $this->createForm(KnowledgeCategoryType::class, null, [
            'SELECTED_CATEGORY_ID' => $this->sessionSelectedKnowledgeCategoryId,
            'SELECTED_CATEGORY_NAME' => $this->sessionSelectedKnowledgeCategoryName,
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var KnowledgeCategory $formData */
            $formData = $form->getData();
            $parent = $this->entityManager->getRepository(KnowledgeCategory::class)
                ->findOneById($this->sessionSelectedKnowledgeCategoryId);

            $formData->setParent($parent);

            $this->getDoctrine()->getManager()->persist($formData);

            $this->getDoctrine()->getManager()->flush();

            $this->setNewCategoryPermission(new KnowledgeCategoryPermission(), $formData);

            return $this->redirectToRoute('app.view_knowledge_category', ['id' => $formData->getId()]);
        }

        throw new \Exception('Can not save form');
    }

    /**
     * @Route("/knowledge/category/{id}/view", name="app.view_knowledge_category", methods={"GET"})
     *
     * @param KnowledgeCategory $knowledgeCategory
     *
     * @return Response
     */
    public function viewCategoryPageAction(KnowledgeCategory $knowledgeCategory)
    {
        $access = $this->checkCategoryAccess($this->getUser(), KnowledgeCategoryPermission::class, $knowledgeCategory
        );

        if (!$access) {
            return $this->render('layouts/no_access_section.html.twig');
        }


        if (!$this->session->isStarted()) {
            $this->session->start();
        } else {
            $this->session->set(self::SELECTED_CATEGORY_ID, $knowledgeCategory->getId());
            $this->session->set(self::SELECTED_CATEGORY_NAME, $knowledgeCategory->getName());
        }

        $knowledgeCategories = $this->entityManager->getRepository(KnowledgeCategory::class)->getChildrenCategories($knowledgeCategory, $this->getUser());

        $lastKnowledgeArticles = $this->entityManager->getRepository(Knowledge::class)
            ->getLastAdded($this->getUser(), self::LIMIT, $knowledgeCategory);

        $this->breadcrumbsAction($knowledgeCategory, []);

        return $this->render('layouts/knowledge/knowledge_category_view.html.twig', [
            'knowledgeCategory' => $knowledgeCategory,
            'categories' => $knowledgeCategories,
            'lastKnowledgeArticles' => $lastKnowledgeArticles,
            'access' => $access,
            'offset' => self::OFFSET,
            'limit' => self::LIMIT,
            'tableId' => 'knowledge-paginator-table',
            'breadcrumbs' => $this->breadcrumbsArray
        ]);
    }

    public function breadcrumbsAction($category, $tree)
    {

        if ($category->getParent()) {
            $tree[$category->getParent()->getId()] = $category->getParent()->getName();
            $this->breadcrumbsAction($category->getParent(), $tree);
        }

        if (!$category->getParent()) {
            $this->breadcrumbsArray = array_reverse(array_flip($tree));
        }
    }

    /**
     * @Route("/knowledge/category/{id}/edit", name="app.edit_knowledge_category_form")
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param KnowledgeCategory $knowledgeCategory
     *
     * @return Response
     */
    public function editFormAction(KnowledgeCategory $knowledgeCategory)
    {
        $form = $this->createForm(KnowledgeCategoryType::class, $knowledgeCategory, [
            'SELECTED_CATEGORY_ID' => $knowledgeCategory->getParent() ? $knowledgeCategory->getParent()->getId() : null,
            'SELECTED_CATEGORY_NAME' => $knowledgeCategory->getParent() ? $knowledgeCategory->getParent()->getName() : null,
            'action' => $this->generateUrl(
                'app.update_knowledge_category', ['id' => $knowledgeCategory->getId()]
            ),
        ]);

        return $this->render('layouts/knowledge/knowledge_category_form.html.twig', [
            'form' => $form->createView(),
            'page_header_text' => 'Редактировать категорию',
            'selectedCategoryId' => $knowledgeCategory->getId(),
            'category' => $knowledgeCategory
        ]);
    }

    /**
     * @Route(
     *     "/knowledge/category/update-knowledge-category/{id}",
     *      name="app.update_knowledge_category"
     *     )
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param KnowledgeCategory $category
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function editFormHandler(KnowledgeCategory $category, Request $request)
    {
        $form = $this->createForm(KnowledgeCategoryType::class, $category, [
            'SELECTED_CATEGORY_ID' => $category->getId(),
            'SELECTED_CATEGORY_NAME' => $category->getName(),
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var KnowledgeCategory $formData */
            $formData = $form->getData();
            $parent = $this->entityManager
                ->getRepository(KnowledgeCategory::class)
                ->findOneById($formData->getParent());
            $formData->setParent($parent);

            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app.view_knowledge_category', ['id' => $category->getId()]);
    }

    /**
     * @Route("/knowledge/category/{id}/remove", name="app.remove_knowledge_category")
     *
     * @IsGranted("ROLE_SUPER_ADMIN")
     *
     * @param KnowledgeCategory $category
     *
     * @return RedirectResponse
     */
    public function removeAction(KnowledgeCategory $category)
    {
        $this->entityManager->remove($category);
        $this->entityManager->flush();
        return $this->redirectToRoute('app.knowledge_index');
    }

    /**
     * @Route("/knowledge/category/{id}/show-next-articles", name="app.show_next_articles", methods={"POST"})
     *
     * @param KnowledgeCategory $category
     * @param Request $request
     * @return Response
     */
    public function showNextPageAction(KnowledgeCategory $category, Request $request)
    {
        $limit = intval($request->request->get('limit'));
        $offset = intval($request->request->get('offset'));

        if ($offset === 0) {
            $offset = $offset + self::OFFSET;
        }

        $tasks = $this->entityManager->getRepository(Knowledge::class)
            ->getLastAdded($this->getUser(), $limit, $category, $offset);

        $offset = $offset + self::OFFSET;

        return $this->render('partials/ajax/paginate_table_body.html.twig', [
            'offset' => $offset,
            'limit' => $limit,
            'contentItems' => $tasks,
            'link_to_content_item' => $this->generateUrl('app.view_task', ['id' => 'id']),
        ]);
    }

    /**
     * @Route("/knowledge/category/{id}/show-prev-articles", name="app.show_prev_articles", methods={"POST"})
     *
     * @param KnowledgeCategory $category
     * @param Request $request
     * @return Response
     */
    public function showPrevPageAction(KnowledgeCategory $category, Request $request)
    {
        $limit = $request->request->get('limit');
        $offset = $request->request->get('offset');
        $offset = $offset - self::OFFSET * 2;

        if ($offset < 0) {
            $offset = 0;
        }

        $tasks = $this->entityManager->getRepository(Knowledge::class)
            ->getLastAdded($this->getUser(), $limit, $category, $offset);

        return $this->render('partials/ajax/paginate_table_body.html.twig', [
            'offset' => $offset,
            'limit' => $limit,
            'contentItems' => $tasks,
            'link_to_content_item' => $this->generateUrl('app.view_task', ['id' => 'id']),
        ]);
    }
}