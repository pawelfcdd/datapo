<?php

namespace App\Form\Task;

use App\Controller\AppController;
use App\Entity\Task;
use App\Entity\TaskCategory;
use App\Form\FieldType\SummernoteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);

        $resolver->setRequired('SELECTED_CATEGORY_NAME');
        $resolver->setRequired('SELECTED_CATEGORY_ID');
        $resolver->setRequired('USER');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'label' => 'Категория',
                'class' => TaskCategory::class,
                'choice_label' => 'name',
                'attr' => [
                    'disabled' => $options['SELECTED_CATEGORY_ID'] ? true : false,
                ],
                'placeholder' => $options['SELECTED_CATEGORY_NAME'],
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Статус',
                'choices' => array_flip(AppController::TASK_STATUSES),
                'expanded' => false,
                'multiple' => false,
                'attr' => [
                    'disabled' => $options['SELECTED_CATEGORY_ID'] ? true : false,
                ]
            ])
            ->add('name', TextType::class, [
                'label' => 'Название задачи	',
                'attr' => [
                    'placeholder' => 'Укажите название'
                ]
            ])
            ->add('description', SummernoteType::class, [
                'label' => 'Текст'
            ])
//                ->add('description', TextareaType::class)
            ->add('attachments', TextType::class, [
                'label' => 'Прикрепить загрузки (ID через запятую)',
                'attr' => [
                    'placeholder' => '1, 2'
                ],
                'required' => false,
                'mapped' => false
            ]);

        if ($options['USER']->hasRole('ROLE_SUPER_ADMIN')) {
            $builder ->add('hidden', ChoiceType::class, [
                'label' => 'Скрытая задача',
                'choices' => [
                    'Да' => 1,
                    'Нет' => 0
                ],
                'expanded' => true,
                'multiple' => false,
            ]);
        }
    }
}