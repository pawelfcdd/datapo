<?php

namespace App\Form\User;

use App\Controller\AppController;
use App\Entity\User;
use Symfony\Component\Form\DataTransformerInterface;

class RolesTransformer implements DataTransformerInterface
{
    public function transform($roles)
    {

        foreach ($roles as $role) {
            if ($role !== User::ROLE_DEFAULT) {
                dump($role);
                return ['DD','SS'];
            }
        }

        dump($roles[0]);
        return $roles[0];
    }

    public function reverseTransform($string)
    {
        return [
            $string, User::ROLE_DEFAULT
        ];
    }
}