<?php

namespace App\Form\Upload;

use App\Entity\Upload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Upload::class
        ]);
        $resolver->setRequired('USER');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Описание',
                'required' => false,
            ])
            ->add('file', FileType::class, [
                'label' => 'Выбрать файл	',
            ]);
           if ($options['USER']->hasRole('ROLE_SUPER_ADMIN')) {
               $builder ->add('hidden', ChoiceType::class, [
                   'label' => 'Скрытая задача',
                   'choices' => [
                       'Да' => 1,
                       'Нет' => 0
                   ],
                   'expanded' => true,
                   'multiple' => false,
               ]);
           }
    }
}