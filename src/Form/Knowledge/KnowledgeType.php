<?php

namespace App\Form\Knowledge;

use App\Entity\Knowledge;
use App\Entity\KnowledgeCategory;
use App\Form\FieldType\SummernoteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KnowledgeType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Knowledge::class
        ]);

        $resolver->setRequired('SELECTED_CATEGORY_NAME');
        $resolver->setRequired('SELECTED_CATEGORY_ID');
        $resolver->setRequired('USER');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'class' => KnowledgeCategory::class,
                'choice_label' => 'name',
                'attr' => [
                    'disabled' => $options['SELECTED_CATEGORY_ID'] ? true : false,
                ],
                'placeholder' => $options['SELECTED_CATEGORY_NAME'],
            ])
            ->add('name', TextType::class, [
                'label' => 'Название задачи	',
                'attr' => [
                    'placeholder' => 'Укажите название'
                ]
            ])
            ->add('description', SummernoteType::class, [
                'label' => 'Текст'
            ])
            ->add('attachments', TextType::class, [
                'label' => 'Прикрепить загрузки (ID через запятую)',
                'attr' => [
                    'placeholder' => '1, 2'
                ],
                'required' => false,
                'mapped' => false
            ]);
        if ($options['USER']->hasRole('ROLE_SUPER_ADMIN')) {
            $builder ->add('hidden', ChoiceType::class, [
                'label' => 'Скрытая задача',
                'choices' => [
                    'Да' => 1,
                    'Нет' => 0
                ],
                'expanded' => true,
                'multiple' => false,
            ]);
        }
    }
}