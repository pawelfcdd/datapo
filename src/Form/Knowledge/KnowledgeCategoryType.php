<?php

namespace App\Form\Knowledge;

use App\Entity\KnowledgeCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KnowledgeCategoryType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KnowledgeCategory::class,
        ]);

        $resolver->setRequired('SELECTED_CATEGORY_NAME');
        $resolver->setRequired('SELECTED_CATEGORY_ID');    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (array_key_exists('data', $options))
        {
            $disabled = false;
        } else {
            $disabled = true;
        }

        $builder
            ->add('parent', EntityType::class, [
                'class' => KnowledgeCategory::class,
                'placeholder' => $options['SELECTED_CATEGORY_NAME'] ? $options['SELECTED_CATEGORY_NAME'] : 'Выберите категорию',
                'required' => false,
                'choice_label' => 'name',
                'attr' => [
                    'disabled' => $disabled,
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'Название категории',
            ])
            ->add('hidden', ChoiceType::class, [
                'label' => 'Скрытая категория',
                'choices' => [
                    'Да' => 1,
                    'Нет' => 0
                ],
                'expanded' => true,
                'multiple' => false,
            ]);    }
}