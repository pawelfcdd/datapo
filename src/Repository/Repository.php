<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class Repository extends EntityRepository
{
    public const USER_ADMIN_ROLE = 'ROLE_SUPER_ADMIN';
    public const USER_EDITOR_ROLE = 'ROLE_EDITOR';
}