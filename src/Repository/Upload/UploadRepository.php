<?php

namespace App\Repository\Upload;

use App\Entity\UploadCategory;
use App\Entity\UploadCategoryPermission;
use App\Entity\User;
use App\Repository\Repository;
use Doctrine\ORM\Query\Expr;

class UploadRepository extends Repository
{
    public const TABLE_ALIAS = 'upload';
    public const CATEGORY_TABLE_ALIAS = 'upload_category';
    public const PERMISSIONS_TABLE_ALIAS = 'category_permissions';

    /**
     * @param User $user
     * @param int|null $limit
     * @param UploadCategory|null $category
     * @param array $categories
     * @return mixed
     */
    public function getLastAdded(
        User $user,
        int $limit = null,
        UploadCategory $category = null,
        array $categories = null
    )
    {
        $query = $this
            ->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(UploadCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = '. self::PERMISSIONS_TABLE_ALIAS .'.category')
            ->leftJoin(UploadCategory::class, self::CATEGORY_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = ' . self::CATEGORY_TABLE_ALIAS . '.id')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS. '.user = :user')
            ->setParameter('user', $user->getId())
            ->setParameter('view', 1)
            ->setParameter('edit', 2);

        if ($category) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category = :category')
                ->setParameter('category', $category);
        }

        if ($categories) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category in(:categories)')
                ->setParameter('categories', $categories);
        }

        if (!$user->hasRole('ROLE_SUPER_ADMIN')) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hiddenUploads')
                ->andWhere(self::CATEGORY_TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hidden', false)
                ->setParameter('hiddenUploads', false);
        }

        if (!$user->hasRole('ROLE_SUPER_ADMIN') && $categories) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category in(:categories)')
                ->setParameter('categories', $categories);
        }

        if (!$user->hasRole('ROLE_SUPER_ADMIN') && $categories) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category in(:categories)')
                ->setParameter('categories', $categories);
        }


        if ($limit) {
            $query
                ->setMaxResults($limit);
        }

        $query->orderBy(self::TABLE_ALIAS . '.id','DESC');

        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param string|null $dateFrom
     * @param string|null $dateTo
     * @param $users
     * @param null $keywords
     * @return mixed
     */
    public function searchTaskResults(
        User $user,
        string $dateFrom = null,
        string $dateTo = null,
        $users = null,
        $keywords = null,
        $uploadId = null
    )
    {
        $query = $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(UploadCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = '. self::PERMISSIONS_TABLE_ALIAS .'.category')
            ->leftJoin(UploadCategory::class, self::CATEGORY_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = ' . self::CATEGORY_TABLE_ALIAS . '.id')
            ->where(self::PERMISSIONS_TABLE_ALIAS. '.user = :user')
            ->setParameter('user', $user->getId());

        if ($dateFrom && $dateTo) {
            $query->andWhere(self::TABLE_ALIAS . '.createdAt BETWEEN :dateFrom AND :dateTo')
                ->setParameter('dateFrom', $dateFrom)
                ->setParameter('dateTo', $dateTo);
        }

        if ($users) {
            $query->andWhere(self::TABLE_ALIAS . '.author IN (:users)')
                ->setParameter('users', $users);
        }

        if ($keywords)
        {
            $query->andWhere(self::TABLE_ALIAS . '.name LIKE :keyword')
                ->setParameter('keyword', '%'.$keywords.'%');
        }

        if ($uploadId)
        {
            $query->andWhere(self::TABLE_ALIAS . '.id LIKE :uploadId')
                ->setParameter('uploadId', $uploadId);
        }

        if (!$user->isSuperAdmin()) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hiddenUpload')
                ->andWhere(self::CATEGORY_TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hiddenUpload', false)
                ->setParameter('hidden', false);
        }
        return $query->getQuery()->getResult();
    }
}