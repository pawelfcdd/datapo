<?php

namespace App\Repository\Upload;

use App\Entity\UploadCategoryPermission;
use App\Entity\User;
use App\Repository\Repository;
use Doctrine\ORM\Query\Expr;

class UploadCategoryRepository extends Repository
{
    public const TABLE_ALIAS = 'upload_category';
    public const PERMISSIONS_TABLE_ALIAS = 'category_permissions';

    /**
     * This method returns a list of all first level tasks categories.
     * Depends of user's role, result can contain hidden categories (for admin) or not (for editor).
     *
     * @param User $user
     *
     * @return mixed
     */
    public function getFirstLevelCategories(User $user)
    {
        $queryBuilder = $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(UploadCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::PERMISSIONS_TABLE_ALIAS . '.category ='.self::TABLE_ALIAS.'.id')
            ->where(self::PERMISSIONS_TABLE_ALIAS . '.user = :user')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit')
            ->andWhere(self::TABLE_ALIAS . '.parent is NULL')
            ->setParameter('view', 1)
            ->setParameter('edit', 2)
            ->setParameter('user', $user->getId());

        if (!$user->isSuperAdmin()) {
            $queryBuilder
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hidden', false);
        }

        $result = $queryBuilder->getQuery()->getResult();

        return $result;
    }

    public function getChildrenCategories(string $parentCagtegoryId, User $user)
    {
        $queryBuilder = $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(UploadCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::PERMISSIONS_TABLE_ALIAS . '.category ='.self::TABLE_ALIAS.'.id')
            ->where(self::PERMISSIONS_TABLE_ALIAS . '.user = :user')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit')
            ->andWhere(self::TABLE_ALIAS . '.parent = :parent')
            ->setParameter('view', 1)
            ->setParameter('edit', 2)
            ->setParameter('user', $user->getId())
            ->setParameter('parent', $parentCagtegoryId);

        if (!$user->isSuperAdmin()) {
            $queryBuilder
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hidden', false);
        }

        $result = $queryBuilder->getQuery()->getResult();

        return $result;

    }

}