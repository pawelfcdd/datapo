<?php

namespace App\Repository\Task;

use App\Controller\AppController;
use App\Entity\TaskCategory;
use App\Entity\TaskCategoryPermission;
use App\Entity\User;
use App\Repository\Repository;
use Doctrine\ORM\Query\Expr;

class TaskRepository extends Repository
{
    public const TABLE_ALIAS = 'task';
    public const CATEGORY_TABLE_ALIAS = 'task_category';
    public const PERMISSIONS_TABLE_ALIAS = 'category_permissions';

    /**
     * @param User $user
     * @param int $limit
     * @param string | array $taskStatus
     * @param TaskCategory|null $category
     * @param int $offset
     * @param array $categories
     * @return mixed
     */
    public function getLastAdded(
        User $user,
        int $limit = null,
        $taskStatus = [],
        TaskCategory $category = null,
        int $offset = null,
        array $categories = null
    )
    {
        $query = $this
            ->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(TaskCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = '. self::PERMISSIONS_TABLE_ALIAS .'.category')
            ->leftJoin(TaskCategory::class, self::CATEGORY_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = ' . self::CATEGORY_TABLE_ALIAS . '.id')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS. '.user = :user')
            ->setParameter('user', $user->getId())
            ->setParameter('view', 1)
            ->setParameter('edit', 2);

        if (!$user->hasRole('ROLE_SUPER_ADMIN')) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hiddenTasks')
                ->andWhere(self::CATEGORY_TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hidden', false)
                ->setParameter('hiddenTasks', false);
        }

        if ($category) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category = :category')
                ->setParameter('category', $category);
        }

        if ($categories) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category in(:categories)')
                ->setParameter('categories', $categories);
        }

        if (is_array($taskStatus)) {
            $statuses = [];

            if (empty($taskStatus)) {
                foreach (AppController::TASK_STATUSES as $key => $val) {
                    $statuses[] = $key;
                }
            } else {
                $statuses = $taskStatus;
            }

            $query
                ->andWhere(self::TABLE_ALIAS . '.status in(:status)')
                ->setParameter('status', $statuses);
        }

        if (is_string($taskStatus)) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.status = :status')
                ->setParameter('status', $taskStatus);
        }

        if (!$user->hasRole('ROLE_SUPER_ADMIN') && $categories) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category in(:categories)')
                ->setParameter('categories', $categories);
        }

        if ($offset) {
            $query->setFirstResult($offset);
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        $query->orderBy(self::TABLE_ALIAS . '.id','DESC');

        return $query->getQuery()->getResult();
    }

    public function taskSearchResults(
        User $user,
        string $dateFrom = null,
        string $dateTo = null,
        $statuses = null,
        $users = null,
        $keywords = null
    )
    {
        $query = $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(TaskCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = '. self::PERMISSIONS_TABLE_ALIAS .'.category')
            ->leftJoin(TaskCategory::class, self::CATEGORY_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = ' . self::CATEGORY_TABLE_ALIAS . '.id')
            ->where(self::PERMISSIONS_TABLE_ALIAS. '.user = :user')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit');

        $parameters = [
            'user' => $user->getId(),
            'view' => 1,
            'edit' => 2
        ];

        if ($dateFrom && $dateTo) {
            $query->andWhere(self::TABLE_ALIAS . '.createdAt BETWEEN :dateFrom AND :dateTo');
            $parameters['dateFrom'] = $dateFrom;
            $parameters['dateTo'] = $dateTo;
        }

        if ($statuses) {
            $query->andWhere(self::TABLE_ALIAS . '.status IN (:statuses)');
            $parameters['statuses'] = $statuses;
        }

        if ($users) {
            $query->andWhere(self::TABLE_ALIAS . '.author IN (:users)');
            $parameters['users'] = $users;
        }

        if ($keywords)
        {
            $query->andWhere(self::TABLE_ALIAS . '.name LIKE :keyword');
            $parameters['keyword'] = '%'.$keywords.'%';
        }

        if (!$user->isSuperAdmin()) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hiddenTask')
                ->andWhere(self::CATEGORY_TABLE_ALIAS . '.hidden = :hidden');
            $parameters['hidden'] = false;
            $parameters['hiddenTask'] = false;
        }

        $query
            ->setParameters($parameters);

        return $query->getQuery()->getResult();
    }
}