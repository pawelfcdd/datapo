<?php

namespace App\Repository\Knowledge;

use App\Entity\Interfaces\CategoryInterface;
use App\Entity\KnowledgeCategoryPermission;
use App\Entity\User;
use App\Repository\Repository;
use Doctrine\ORM\Query\Expr;

class KnowledgeCategoryRepository extends Repository
{
    public const TABLE_ALIAS = 'knowledge_category';
    public const PERMISSIONS_TABLE_ALIAS = 'category_permissions';

    /**
     * @param User $user
     * @return mixed
     */
    public function getFirstLevelCategories(User $user)
    {
        $queryBuilder = $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(KnowledgeCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::PERMISSIONS_TABLE_ALIAS . '.category ='.self::TABLE_ALIAS.'.id')
            ->where(self::PERMISSIONS_TABLE_ALIAS . '.user = :user')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit')
            ->andWhere(self::TABLE_ALIAS . '.parent is NULL')
            ->setParameter('view', 1)
            ->setParameter('edit', 2)
            ->setParameter('user', $user->getId());

        if (!$user->isSuperAdmin()) {
            $queryBuilder
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hidden', false);
        }

        $result = $queryBuilder->getQuery()->getResult();

        return $result;
    }

    public function getChildrenCategories(CategoryInterface $category, User $user)
    {
        $queryBuilder = $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(KnowledgeCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::PERMISSIONS_TABLE_ALIAS . '.category ='.self::TABLE_ALIAS.'.id')
            ->where(self::PERMISSIONS_TABLE_ALIAS . '.user = :user')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit')
            ->andWhere(self::TABLE_ALIAS . '.parent = :parent')
            ->setParameter('view', 1)
            ->setParameter('edit', 2)
            ->setParameter('user', $user->getId())
            ->setParameter('parent', $category->getId());

        if (!$user->isSuperAdmin()) {
            $queryBuilder
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hidden', false);
        }

        $result = $queryBuilder->getQuery()->getResult();

        return $result;

    }
}