<?php

namespace App\Repository\Knowledge;

use App\Entity\KnowledgeCategory;
use App\Entity\KnowledgeCategoryPermission;
use App\Entity\User;
use App\Repository\Repository;
use Doctrine\ORM\Query\Expr;

class KnowledgeRepository extends Repository
{
    public const TABLE_ALIAS = 'knowledge_articles';
    public const CATEGORY_TABLE_ALIAS = 'knowledge_category';
    public const PERMISSIONS_TABLE_ALIAS = 'category_permissions';

    /**
     * @param User $user
     * @param int $limit
     * @param KnowledgeCategory|null $category
     * @param int|null $offset
     * @return mixed
     */
    public function getLastAdded(
        User $user,
        int $limit = 10,
        KnowledgeCategory $category = null,
        int $offset = null,
        array $categories = null
    )
    {
        $query = $this
            ->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(KnowledgeCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = '. self::PERMISSIONS_TABLE_ALIAS .'.category')
            ->leftJoin(KnowledgeCategory::class, self::CATEGORY_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = ' . self::CATEGORY_TABLE_ALIAS . '.id')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS. '.user = :user')
            ->setParameter('user', $user->getId())
            ->setParameter('view', 1)
            ->setParameter('edit', 2);

        if ($category) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category = :category')
                ->setParameter('category', $category);
        }

        if ($offset) {
            $query->setFirstResult($offset);
        }

        if (!$user->hasRole('ROLE_SUPER_ADMIN')) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hiddenArticles')
                ->andWhere(self::CATEGORY_TABLE_ALIAS . '.hidden = :hidden')
                ->setParameter('hidden', false)
                ->setParameter('hiddenArticles', false);
        }

        if (!$user->hasRole('ROLE_SUPER_ADMIN') && $categories) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.category in(:categories)')
                ->setParameter('categories', $categories);
        }

        $query
            ->setMaxResults($limit)
            ->orderBy(self::TABLE_ALIAS . '.id','DESC');

        return $query->getQuery()->getResult();
    }

    /**
     * @param string $dateFrom
     * @param string $dateTo
     * @param $users
     * @param null $keywords
     * @return mixed
     */
    public function searchTaskResults(
        User $user,
        string $dateFrom = null,
        string $dateTo = null,
        $users = null,
        $keywords = null
    )
    {
        $query = $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select()
            ->leftJoin(KnowledgeCategoryPermission::class, self::PERMISSIONS_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = '. self::PERMISSIONS_TABLE_ALIAS .'.category')
            ->leftJoin(KnowledgeCategory::class, self::CATEGORY_TABLE_ALIAS, Expr\Join::WITH, self::TABLE_ALIAS . '.category = ' . self::CATEGORY_TABLE_ALIAS . '.id')
            ->where(self::PERMISSIONS_TABLE_ALIAS. '.user = :user')
            ->andWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :view')
            ->orWhere(self::PERMISSIONS_TABLE_ALIAS . '.accessLevel = :edit');

        $parameters = [
            'user' => $user->getId(),
            'view' => 1,
            'edit' => 2
        ];

        if ($dateFrom && $dateTo) {
            $query->andWhere(self::TABLE_ALIAS . '.createdAt BETWEEN :dateFrom AND :dateTo');
            $parameters['dateFrom'] = $dateFrom;
            $parameters['dateTo'] = $dateTo;
        }
        if ($users) {
            $query->andWhere(self::TABLE_ALIAS . '.author IN (:users)');
            $parameters['users'] = $users;
        }

        if ($keywords)
        {
            $query->andWhere(self::TABLE_ALIAS . '.name LIKE :keyword');
            $parameters['keyword'] = '%'.$keywords.'%';
        }

        if (!$user->isSuperAdmin()) {
            $query
                ->andWhere(self::TABLE_ALIAS . '.hidden = :hiddenKnowledge')
                ->andWhere(self::CATEGORY_TABLE_ALIAS . '.hidden = :hidden');
            $parameters['hidden'] = false;
            $parameters['hiddenKnowledge'] = false;
        }
        $query->orderBy(self::TABLE_ALIAS . '.id','DESC');

        $query
            ->setParameters($parameters);

        return $query->getQuery()->getResult();
    }
}