<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190123142207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE knowledge_articles (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, author_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(10000) NOT NULL, hidden TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, editor INT DEFAULT NULL, edit_at DATETIME DEFAULT NULL, INDEX IDX_C2A5E8F712469DE2 (category_id), INDEX IDX_C2A5E8F7F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE knowledge_categories (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, hidden TINYINT(1) NOT NULL, INDEX IDX_3325C7B5727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE knowledge_articles ADD CONSTRAINT FK_C2A5E8F712469DE2 FOREIGN KEY (category_id) REFERENCES knowledge_categories (id)');
        $this->addSql('ALTER TABLE knowledge_articles ADD CONSTRAINT FK_C2A5E8F7F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE knowledge_categories ADD CONSTRAINT FK_3325C7B5727ACA70 FOREIGN KEY (parent_id) REFERENCES knowledge_categories (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE knowledge_articles DROP FOREIGN KEY FK_C2A5E8F712469DE2');
        $this->addSql('ALTER TABLE knowledge_categories DROP FOREIGN KEY FK_3325C7B5727ACA70');
        $this->addSql('DROP TABLE knowledge_articles');
        $this->addSql('DROP TABLE knowledge_categories');
    }
}
