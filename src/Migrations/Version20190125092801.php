<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190125092801 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE knowledge_attachments (knowledge_id INT NOT NULL, attachment_id INT NOT NULL, INDEX IDX_BFAF823EE7DC6902 (knowledge_id), INDEX IDX_BFAF823E464E68B (attachment_id), PRIMARY KEY(knowledge_id, attachment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE knowledge_attachments ADD CONSTRAINT FK_BFAF823EE7DC6902 FOREIGN KEY (knowledge_id) REFERENCES knowledge_articles (id)');
        $this->addSql('ALTER TABLE knowledge_attachments ADD CONSTRAINT FK_BFAF823E464E68B FOREIGN KEY (attachment_id) REFERENCES uploads (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE knowledge_attachments');
    }
}
