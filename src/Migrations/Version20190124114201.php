<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190124114201 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE uploads (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, author_id INT DEFAULT NULL, full_file_name VARCHAR(100) NOT NULL, name VARCHAR(100) NOT NULL, description VARCHAR(10000) NOT NULL, hidden TINYINT(1) NOT NULL, file VARCHAR(255) NOT NULL, INDEX IDX_96117F1812469DE2 (category_id), INDEX IDX_96117F18F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE uploads ADD CONSTRAINT FK_96117F1812469DE2 FOREIGN KEY (category_id) REFERENCES upload_categories (id)');
        $this->addSql('ALTER TABLE uploads ADD CONSTRAINT FK_96117F18F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE uploads');
    }
}
