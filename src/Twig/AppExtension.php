<?php

namespace App\Twig;

use App\Controller\AppController;
use App\Entity\KnowledgeCategoryPermission;
use App\Entity\KnowledgeSectionPermission;
use App\Entity\Task;
use App\Entity\TaskCategory;
use App\Entity\TaskCategoryPermission;
use App\Entity\TaskSectionPermission;
use App\Entity\Upload;
use App\Entity\UploadCategory;
use App\Entity\UploadCategoryPermission;
use App\Entity\UploadsSectionPermission;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    private $entityManager;
    private $container;
    private $tasks = [];

    public const TASK_CATEGORY_PERMISSION_PATTERN = 'task_category_';
    public const TASK_SECTION_PERMISSION_PATTERN = 'tasks_section_';

    public const UPLOAD_SECTION_PERMISSION_PATTERN = 'uploads_section_';
    public const UPLOAD_CATEGORY_PERMISSION_PATTERN = 'upload_category_';

    public const KNOWLEDGE_CATEGORY_PERMISSION_PATTERN = 'knowledge_category_';
    public const KNOWLEDGE_SECTION_PERMISSION_PATTERN = 'knowledge_section_';

    public const PERMISSION_CLASSES = [
        'task_category_' => TaskCategoryPermission::class,
        'upload_category_' => UploadCategoryPermission::class,
        'knowledge_category_' => KnowledgeCategoryPermission::class,
    ];

    public const SECTION_PERMISSION_CLASSES = [
        'tasks_section_' => TaskSectionPermission::class,
        'uploads_section_' => UploadsSectionPermission::class,
        'knowledge_section_' => KnowledgeSectionPermission::class,
    ];

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('count_tasks', [$this, 'countTaskCategoryTasks']),
            new TwigFunction('get_users', [$this, 'getUsers']),
            new TwigFunction('get_category_permission_code', [$this, 'getCategoryPermissionCode']),
            new TwigFunction('upload_category_attachment', [$this, 'getUploadCategoryAttachments']),
            new TwigFunction('get_auth_key', [$this, 'getAuthKey']),

        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('task_section_button', [$this, 'tasksSectionButton']),
            new TwigFilter('knowledge_section_button', [$this, 'knowledgeSectionButton']),
            new TwigFilter('uploads_section_button', [$this, 'uploadsSectionPermission']),
        ];
    }

    public function fillChain($category, $user)
    {
        if (!$category->getChildren()->isEmpty()) {

            switch ($user->hasRole('ROLE_SUPER_ADMIN')) {
                case true:
                    foreach ($category->getChildren() as $child) {
                        $this->tasks[] = $child->getId();
                        $this->fillChain($child, $user);
                    }
                    break;
                case false:
                    if (!$category->isHidden()) {
                        foreach ($category->getChildren() as $child) {
                            if (!$child->isHidden()) {
                                $this->tasks[] = $child->getId();
                            }
                            $this->fillChain($child, $user);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * @param TaskCategory $category
     * @param string $taskStatus
     * @param User $user
     * @return int
     */
    public function countTaskCategoryTasks(TaskCategory $category, string $taskStatus, User $user)
    {
        $this->tasks[] = $category->getId();

        $this->fillChain($category, $user);

        $tasks = $this->entityManager->getRepository(Task::class)
            ->getLastAdded($user, null, $taskStatus, null, null, $this->tasks);
        $this->tasks = [];

        return count($tasks);
    }

    /**
     * @return User[]|object[]
     */
    public function getUsers()
    {
        $users = $this->entityManager->getRepository(User::class)->findAll();

        return $users;
    }

    /**
     * @param User|null $user
     * @param string $categoryNameId
     * @return null
     */
    public function getCategoryPermissionCode(User $user = null, string $categoryNameId)
    {
        if (!$user) {
            return null;
        }

        if (is_int(strpos($categoryNameId, self::TASK_SECTION_PERMISSION_PATTERN))) {
            $categoryPermissionClass = self::SECTION_PERMISSION_CLASSES[self::TASK_SECTION_PERMISSION_PATTERN];
            $permission = $this->entityManager->getRepository($categoryPermissionClass)->findOneBy([
                'user' => $user->getId(),
            ]);

            return $permission ? $permission->getAccessLevel() : null;
        }

        if (is_int(strpos($categoryNameId, self::UPLOAD_SECTION_PERMISSION_PATTERN))) {
            $categoryPermissionClass = self::SECTION_PERMISSION_CLASSES[self::UPLOAD_SECTION_PERMISSION_PATTERN];
            $permission = $this->entityManager->getRepository($categoryPermissionClass)->findOneBy([
                'user' => $user->getId(),
            ]);

            return $permission ? $permission->getAccessLevel() : null;
        }

        if (is_int(strpos($categoryNameId, self::KNOWLEDGE_SECTION_PERMISSION_PATTERN))) {
            $categoryPermissionClass = self::SECTION_PERMISSION_CLASSES[self::KNOWLEDGE_SECTION_PERMISSION_PATTERN];
            $permission = $this->entityManager->getRepository($categoryPermissionClass)->findOneBy([
                'user' => $user->getId(),
            ]);

            return $permission ? $permission->getAccessLevel() : null;
        }

        if (is_int(strpos($categoryNameId, self::TASK_CATEGORY_PERMISSION_PATTERN))) {
            $categoryPermissionClass = self::PERMISSION_CLASSES[self::TASK_CATEGORY_PERMISSION_PATTERN];
            $categoryId = intval(str_replace(self::TASK_CATEGORY_PERMISSION_PATTERN, '', $categoryNameId));
            $permission = $this->entityManager->getRepository($categoryPermissionClass)->findOneBy([
                'user' => $user->getId(),
                'category' => $categoryId
            ]);

            return $permission ? $permission->getAccessLevel() : null;
        }

        if (is_int(strpos($categoryNameId, self::KNOWLEDGE_CATEGORY_PERMISSION_PATTERN))) {
            $categoryPermissionClass = self::PERMISSION_CLASSES[self::KNOWLEDGE_CATEGORY_PERMISSION_PATTERN];
            $categoryId = intval(str_replace(self::KNOWLEDGE_CATEGORY_PERMISSION_PATTERN, '', $categoryNameId));
            $permission = $this->entityManager->getRepository($categoryPermissionClass)->findOneBy([
                'user' => $user->getId(),
                'category' => $categoryId
            ]);

            return $permission ? $permission->getAccessLevel() : null;
        }

        if (is_int(strpos($categoryNameId, self::UPLOAD_CATEGORY_PERMISSION_PATTERN))) {
            $categoryPermissionClass = self::PERMISSION_CLASSES[self::UPLOAD_CATEGORY_PERMISSION_PATTERN];
            $categoryId = intval(str_replace(self::UPLOAD_CATEGORY_PERMISSION_PATTERN, '', $categoryNameId));
            $permission = $this->entityManager->getRepository($categoryPermissionClass)->findOneBy([
                'user' => $user->getId(),
                'category' => $categoryId
            ]);

            return $permission ? $permission->getAccessLevel() : null;
        }
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        $authKeyParameter = Yaml::parseFile($this->container->getParameter('files_authkey'));

        return $authKeyParameter;
    }

    public function getUploadCategoryAttachments(UploadCategory $category, User $user)
    {
        $this->tasks[] = $category->getId();
        $this->fillChain($category, $user);

        $uploads = $this->entityManager->getRepository(Upload::class)
            ->getLastAdded($user, null, null, $this->tasks);

        $this->tasks = [];

        return count($uploads);
    }

    private function getSubcategoryAttachment(UploadCategory $category, array $attachments = null, User $user)
    {
        /** @var UploadCategory $child */
        foreach ($category->getChildren() as $child) {
            if (!empty($child->getUploads())) {
                /** @var Upload $upload */
                foreach ($child->getUploads() as $upload) {
                    if ($upload->isHidden()) {
                        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
                            $attachments[] = $upload;
                        }
                    } else {
                        if ($upload->getCategory()->isHidden()) {
                            if ($user->hasRole('ROLE_SUPER_ADMIN')) {
                                $attachments[] = $upload;
                            }
                        } else {
                            $attachments[] = $upload;
                        }
                    }
                }
            }

            if (!empty($child->getChildren())) {
                $this->getSubcategoryAttachment($child, $attachments, $user);
            }
        }

        return $attachments;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function tasksSectionButton(User $user)
    {
        $sectionPermission = $this->entityManager->getRepository(TaskSectionPermission::class)
            ->findOneByUser($user->getId());

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        if ($sectionPermission->getAccessLevel() == 0) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function knowledgeSectionButton(User $user)
    {
        $sectionPermission = $this->entityManager->getRepository(KnowledgeSectionPermission::class)
            ->findOneByUser($user->getId());

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        if ($sectionPermission->getAccessLevel() == 0) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function uploadsSectionPermission(User $user)
    {
        $sectionPermission = $this->entityManager->getRepository(UploadsSectionPermission::class)
            ->findOneByUser($user->getId());

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        if ($sectionPermission->getAccessLevel() == 0) {
            return false;
        }

        return true;
    }

    public function categoriesListAction()
    {
        $categories = $this->entityManager->getRepository(TaskCategory::class)->findAll();
        $categoriesTree = [];
        foreach ($categories as $category) {
            if (!$category->getParent()) {
                $categoriesTree[$category->getId()]['children'] = [];
            }
        }
        $tree = $this->renderNestedTree($categories, $categoriesTree);

        return $tree;
    }

    private function renderNestedTree(array $categories, array $categoriesTree)
    {
        //TODO: Ta metoda buduje drzewo kategorii używajac rekurencji
        foreach ($categories as $category) {
            if ($category->getParent()) {
                if (array_key_exists($category->getParent()->getId(), $categoriesTree)) {
                    $parent = $category->getParent();
                    $categoriesTree[$parent->getId()]['children'][$category->getId()]['children'] = [];
                }
            }
        }
        foreach ($categoriesTree as $key => $treeItem) {
            if (!empty($treeItem['children'])) {
                $children = $this->renderNestedTree($categories, $treeItem['children']);
                $categoriesTree[$key]['children'] = $children;
            }
        }
        return $categoriesTree;
    }
}