<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Knowledge\KnowledgeRepository")
 * @ORM\Table(name="knowledge_articles")
 * @ORM\HasLifecycleCallbacks()
 */
class Knowledge
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="KnowledgeCategory", inversedBy="knowledgeCollection")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @var string
     * @ORM\Column()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(length=10000)
     */
    private $description;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="knowledge")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $author;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $createdAt;

    /**
     * @var integer
     * @ORM\Column(type="integer", length=20, nullable=true)
     */
    private $editor;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $editAt;

    /**
     * @ORM\ManyToMany(targetEntity="Upload")
     * @ORM\JoinTable(name="knowledge_attachments",
     *      joinColumns={@ORM\JoinColumn(name="knowledge_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="attachment_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $attachments;

    public function __construct()
    {
        $this->attachments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     *
     * @return Knowledge
     */
    public function setCategory($category): Knowledge
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Knowledge
     */
    public function setName(string $name): Knowledge
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Knowledge
     */
    public function setDescription(string $description): Knowledge
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return Knowledge
     */
    public function setHidden(bool $hidden): Knowledge
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Knowledge
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getEditor(): int
    {
        return $this->editor;
    }

    /**
     * @param int $editor
     * @return Knowledge
     */
    public function setEditor(int $editor): Knowledge
    {
        $this->editor = $editor;
        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setEditAt()
    {
        $this->editAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getEditAt()
    {
        return $this->editAt;
    }

    public function addAttachment(Upload $upload)
    {
        if (!$this->attachments->contains($upload)) {
            $this->attachments->add($upload);
        }

        return $this;
    }

    public function removeAttachment(Upload $upload)
    {
        if ($this->attachments->contains($upload)) {
            $this->attachments->removeElement($upload);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
}