<?php

namespace App\Entity;

use App\Entity\Interfaces\CategoryInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Knowledge\KnowledgeCategoryRepository")
 * @ORM\Table(name="knowledge_categories")
 */
class KnowledgeCategory implements CategoryInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="KnowledgeCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="KnowledgeCategory", mappedBy="parent")
     */
    private $children;

    /**
     * @var string
     * @ORM\Column()
     */
    private $name;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @ORM\OneToMany(targetEntity="Knowledge", mappedBy="category")
     */
    private $knowledgeCollection;

    public function __construct()
    {
        $this->knowledgeCollection = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return KnowledgeCategory
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return KnowledgeCategory
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return KnowledgeCategory
     */
    public function setName(string $name): KnowledgeCategory
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return KnowledgeCategory
     */
    public function setHidden(bool $hidden): KnowledgeCategory
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @param Knowledge $knowledge
     * @return $this
     */
    public function addKnowledge(Knowledge $knowledge)
    {
        if (!$this->knowledgeCollection->contains($knowledge))
        {
            $this->knowledgeCollection->add($knowledge);
        }

        return $this;
    }

    /**
     * @param Knowledge $knowledge
     * @return $this
     */
    public function removeKnowledge(Knowledge $knowledge)
    {
        if ($this->knowledgeCollection->contains($knowledge))
        {
            $this->knowledgeCollection->remove($knowledge);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKnowledgeCollection()
    {
        return $this->knowledgeCollection;
    }

}