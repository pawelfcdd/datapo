<?php

namespace App\Entity;

use App\Entity\Interfaces\CategoryPermissionInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="task_category_permissions")
 */
class TaskCategoryPermission implements CategoryPermissionInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $category;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $accessLevel;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): ?int
    {
        return $this->user;
    }

    /**
     * @param int $user
     * @return TaskCategoryPermission
     */
    public function setUser(int $user): TaskCategoryPermission
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }

    /**
     * @param int $category
     * @return TaskCategoryPermission
     */
    public function setCategory(int $category): TaskCategoryPermission
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccessLevel(): ?int
    {
        return $this->accessLevel;
    }

    /**
     * @param int $accessLevel
     * @return TaskCategoryPermission
     */
    public function setAccessLevel(int $accessLevel): TaskCategoryPermission
    {
        $this->accessLevel = $accessLevel;
        return $this;
    }
}