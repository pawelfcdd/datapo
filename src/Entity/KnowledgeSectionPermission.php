<?php

namespace App\Entity;

use App\Entity\Interfaces\SectionPermissionInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="knowledge_section_permissions")
 */

class KnowledgeSectionPermission implements SectionPermissionInterface
{
    /**
     * @var integer | null
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer | null
     * @ORM\Column(type="integer")
     */
    private $user;

    /**
     * @var integer | null
     * @ORM\Column(type="integer")
     */
    private $accessLevel;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getUser(): ?int
    {
        return $this->user;
    }

    /**
     * @param int|null $user
     * @return KnowledgeSectionPermission
     */
    public function setUser(?int $user): KnowledgeSectionPermission
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAccessLevel(): ?int
    {
        return $this->accessLevel;
    }

    /**
     * @param int|null $accessLevel
     * @return KnowledgeSectionPermission
     */
    public function setAccessLevel(?int $accessLevel): KnowledgeSectionPermission
    {
        $this->accessLevel = $accessLevel;
        return $this;
    }
}