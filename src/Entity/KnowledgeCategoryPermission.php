<?php

namespace App\Entity;

use App\Entity\Interfaces\CategoryPermissionInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="knowledge_category_permissions")
 */
class KnowledgeCategoryPermission implements CategoryPermissionInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $category;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $accessLevel;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): ?int
    {
        return $this->user;
    }

    /**
     * @param int $user
     * @return KnowledgeCategoryPermission
     */
    public function setUser(int $user): KnowledgeCategoryPermission
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }

    /**
     * @param int $category
     * @return KnowledgeCategoryPermission
     */
    public function setCategory(int $category): KnowledgeCategoryPermission
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccessLevel(): ?int
    {
        return $this->accessLevel;
    }

    /**
     * @param int $accessLevel
     * @return KnowledgeCategoryPermission
     */
    public function setAccessLevel(int $accessLevel): KnowledgeCategoryPermission
    {
        $this->accessLevel = $accessLevel;
        return $this;
    }
}