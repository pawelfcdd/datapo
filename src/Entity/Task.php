<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskRepository")
 * @ORM\Table(name="tasks")
 * @ORM\HasLifecycleCallbacks()
 */
class Task
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TaskCategory", inversedBy="tasks")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @var string
     * @ORM\Column()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(length=10000)
     */
    private $description;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @var string
     * @ORM\Column()
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tasks")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $createdAt;

    /**
     * @var integer
     * @ORM\Column(type="integer", length=20, nullable=true)
     */
    private $editor;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $editAt;

    /**
     * @ORM\ManyToMany(targetEntity="Upload")
     * @ORM\JoinTable(name="tasks_attachments",
     *      joinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="attachment_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $attachments;

    /**
     * @ORM\OneToMany(targetEntity="TaskComment", mappedBy="task")
     */
    private $comments;

    public function __construct()
    {
        $this->attachments = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Task
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Task
     */
    public function setName(string $name): Task
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Task
     */
    public function setDescription(string $description): Task
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return Task
     */
    public function setHidden(bool $hidden): Task
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Task
     */
    public function setStatus(string $status): Task
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getEditor(): ?int
    {
        return $this->editor;
    }

    /**
     * @param int $editor
     * @return Task
     */
    public function setEditor(int $editor): Task
    {
        $this->editor = $editor;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setEditAt()
    {
        $this->editAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getEditAt()
    {
        return $this->editAt;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Task
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    public function addAttachment(Upload $upload)
    {
        if (!$this->attachments->contains($upload)) {
            $this->attachments->add($upload);
        }

        return $this;
    }

    public function removeAttachment(Upload $upload)
    {
        if ($this->attachments->contains($upload)) {
            $this->attachments->removeElement($upload);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    public function addComment(TaskComment $comment)
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
        }

        return $this;
    }

    public function removeComment(TaskComment $comment)
    {
        if ($this->comments->contains($comment)) {
            $this->comments->remove($comment);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }
}