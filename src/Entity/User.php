<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="author")
     */
    private $tasks;

    /**
     * @ORM\OneToMany(targetEntity="Knowledge", mappedBy="author")
     */
    private $knowledge;

    /**
     * @ORM\OneToMany(targetEntity="Upload", mappedBy="author")
     */
    private $uploads;

    /**
     * @ORM\OneToMany(targetEntity="TaskComment", mappedBy="user")
     */
    private $comments;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->tasks = new ArrayCollection();
        $this->knowledge = new ArrayCollection();
        $this->uploads = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setDeletedOnCreate(): User
    {
        $this->deleted = false;
        return $this;
    }

    /**
     * @param $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }
}