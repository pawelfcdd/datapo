<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Upload\UploadRepository")
 * @ORM\Table(name="uploads")
 * @ORM\HasLifecycleCallbacks()
 */
class Upload
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UploadCategory", inversedBy="uploads")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @var string
     * @ORM\Column(length=100)
     */
    private $fullFileName;

    /**
     * @var string
     * @ORM\Column(length=100)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(length=10000, nullable=true)
     */
    private $description;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="uploads")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(message="File was not selected")
     * @Assert\File(mimeTypes={
     *     "image/png",
     *     "image/gif",
     *     "image/jpeg",
     *     "application/pdf",
     *     "application/zip",
     *     "application/x-rar-compressed"
     * })
     */
    private $file;

    /**
     * @var string
     * @ORM\Column()
     */
    private $extension;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    public $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Upload
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullFileName(): ?string
    {
        return $this->fullFileName;
    }

    /**
     * @param string $fullFileName
     * @return Upload
     */
    public function setFullFileName(string $fullFileName): Upload
    {
        $this->fullFileName = $fullFileName;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Upload
     */
    public function setName(string $name): Upload
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Upload
     */
    public function setDescription(string $description = null): Upload
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return Upload
     */
    public function setHidden(bool $hidden): Upload
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Upload
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return Upload
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     * @return Upload
     */
    public function setExtension(string $extension): Upload
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * @return $this
     */
    public function getCreatedAt()
    {
        return $this;
    }
}