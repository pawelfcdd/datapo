<?php

namespace App\Entity;

use App\Entity\Interfaces\CategoryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Upload\UploadCategoryRepository")
 * @ORM\Table(name="upload_categories")
 */
class UploadCategory implements CategoryInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UploadCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="UploadCategory", mappedBy="parent")
     */
    private $children;

    /**
     * @var string
     * @ORM\Column()
     */
    private $name;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @ORM\OneToMany(targetEntity="Upload", mappedBy="category")
     */
    private $uploads;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return UploadCategory
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return UploadCategory
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UploadCategory
     */
    public function setName(string $name): UploadCategory
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return UploadCategory
     */
    public function setHidden(bool $hidden): UploadCategory
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUploads()
    {
        return $this->uploads;
    }

    /**
     * @param mixed $uploads
     * @return UploadCategory
     */
    public function setUploads($uploads)
    {
        $this->uploads = $uploads;
        return $this;
    }
}