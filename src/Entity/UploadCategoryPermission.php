<?php

namespace App\Entity;

use App\Entity\Interfaces\CategoryPermissionInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="upload_category_permissions")
 */
class UploadCategoryPermission implements CategoryPermissionInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $category;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $accessLevel;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): ?int
    {
        return $this->user;
    }

    /**
     * @param int $user
     * @return UploadCategoryPermission
     */
    public function setUser(int $user): UploadCategoryPermission
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }

    /**
     * @param int $category
     * @return UploadCategoryPermission
     */
    public function setCategory(int $category): UploadCategoryPermission
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccessLevel(): ?int
    {
        return $this->accessLevel;
    }

    /**
     * @param int $accessLevel
     * @return UploadCategoryPermission
     */
    public function setAccessLevel(int $accessLevel): UploadCategoryPermission
    {
        $this->accessLevel = $accessLevel;
        return $this;
    }
}