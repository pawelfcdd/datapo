<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="task_comments")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskComment
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="comments")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string|null
     * @ORM\Column(type="text", length=500)
     */
    private $userComment;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean")
     */
    private $isAuthor;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     * @return TaskComment
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return TaskComment
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUserComment(): ?string
    {
        return $this->userComment;
    }

    /**
     * @param null|string $userComment
     * @return TaskComment
     */
    public function setUserComment(?string $userComment): TaskComment
    {
        $this->userComment = $userComment;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getisAuthor(): ?bool
    {
        return $this->isAuthor;
    }

    /**
     * @param bool|null $isAuthor
     * @return TaskComment
     */
    public function setIsAuthor(?bool $isAuthor): TaskComment
    {
        $this->isAuthor = $isAuthor;
        return $this;
    }
}