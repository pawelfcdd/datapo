<?php

namespace App\Entity;

use App\Entity\Interfaces\CategoryInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Task\TaskCategoryRepository")
 * @ORM\Table(name="tasks_categories")
 */
class TaskCategory implements CategoryInterface
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TaskCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="TaskCategory", mappedBy="parent")
     */
    private $children;

    /**
     * @var string
     * @ORM\Column()
     */
    private $name;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="category", cascade={"remove"}, orphanRemoval=true)
     */
    private $tasks;

    /**
     * TaskCategory constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->tasks = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return TaskCategory
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TaskCategory
     */
    public function setName(string $name): TaskCategory
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return TaskCategory
     */
    public function setHidden(bool $hidden): TaskCategory
    {
        $this->hidden = $hidden;
        return $this;
    }

    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param TaskCategory $taskCategory
     * @return TaskCategory
     */
    public function addChildren(TaskCategory $taskCategory)
    {
        if (!$this->children->contains($taskCategory)) {
            $this->children->add($taskCategory);
        }

        return $this;
    }

    /**
     * @param TaskCategory $taskCategory
     * @return TaskCategory
     */
    public function removeChildren(TaskCategory $taskCategory)
    {
        if ($this->children->contains($taskCategory)) {
            $this->children->remove($taskCategory);
        }

        return $this;
    }

    /**
     * @param Task $task
     * @return $this
     */
    public function setTask(Task $task)
    {
        if (!$this->tasks->contains($task))
        {
            $this->tasks->add($task);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param Task $task
     * @return $this
     */
    public function removeTask(Task $task)
    {
        if ($this->tasks->contains($task))
        {
            $this->tasks->remove($task);
        }

        return $this;
    }
}